package edu.jhu.apl.patterns_class;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.jhu.apl.patterns_class.dom.ConcreteDOMBuilder;
import edu.jhu.apl.patterns_class.dom.ConcreteDOMFactory;
import edu.jhu.apl.patterns_class.dom.DOMDirector;
import edu.jhu.apl.patterns_class.dom.Element;
import edu.jhu.apl.patterns_class.dom.Node;
import edu.jhu.apl.patterns_class.dom.NodeCompositeInterface;
import edu.jhu.apl.patterns_class.dom.NodeIterator;
import edu.jhu.apl.patterns_class.support.MockGUI;
import edu.jhu.apl.patterns_class.support.interpreter.*;

/**
 * Reads an XML file and extracts the simple tokens to simplify converting the
 * file into the representative DOM structure.
 */
public class XMLTokenizer {
	private BufferedReader reader = null;
	private String line = null;
	private int line_number = 0;
	private int index = 0;
	private boolean inside_tag = false;
	private String filename = null;

	private static final Pattern PROLOG_START = Pattern.compile("<\\?");
	private static final Pattern PROLOG_IDENTIFIER = Pattern.compile("\\p{Space}*xml");
	private static final Pattern ATTRIBUTE_VALUE = Pattern.compile("\"[^\"]*\"");
	private static final Pattern PROLOG_END = Pattern.compile("\\p{Space}*\\?>");
	private static final Pattern TAG_START = Pattern.compile("\\p{Space}*<");
	private static final Pattern ELEMENT = Pattern.compile("\\p{Space}*\\p{Alpha}([\\p{Alnum}_-]|:)*");
	private static final Pattern ATTRIBUTE = Pattern.compile("\\p{Space}+\\p{Alpha}([\\p{Alnum}_-]|:)*\\p{Space}*=");
	private static final Pattern NULL_TAG_END = Pattern.compile("\\p{Space}*/>");
	private static final Pattern TAG_CLOSE_START = Pattern.compile("\\p{Space}*</");
	private static final Pattern VALUE = Pattern.compile("[^<]*");
	private static final Pattern TAG_END = Pattern.compile("\\p{Space}*>");
	private static final Pattern SPACE_TO_EOL = Pattern.compile("\\p{Space}*");

	private Matcher prolog_start_matcher;
	private Matcher prolog_identifier_matcher;
	private Matcher attribute_value_matcher;
	private Matcher prolog_end_matcher;
	private Matcher tag_start_matcher;
	private Matcher element_matcher;
	private Matcher attribute_matcher;
	private Matcher null_tag_end_matcher;
	private Matcher tag_close_start_matcher;
	private Matcher value_matcher;
	private Matcher tag_end_matcher;
	private Matcher space_to_eol_matcher;

	public class XMLToken {
		public static final int NULL = 0;
		public static final int PROLOG_START = 1;
		public static final int PROLOG_IDENTIFIER = 2;
		public static final int ATTRIBUTE_VALUE = 3;
		public static final int PROLOG_END = 4;
		public static final int TAG_START = 5;
		public static final int ELEMENT = 6;
		public static final int ATTRIBUTE = 7;
		public static final int NULL_TAG_END = 8;
		public static final int TAG_CLOSE_START = 9;
		public static final int VALUE = 10;
		public static final int TAG_END = 11;

		private String token = null;
		private int token_type = NULL;

		XMLToken(String t, int tt) {
			token = t;
			token_type = tt;
		}

		public String getToken() {
			return token;
		}

		public int getTokenType() {
			return token_type;
		}

		// TODO: Pattern Opportunity: Factory Method, maybe Template Method.
		public String toString() {
			switch (token_type) {
			case NULL:
				return "NULL";
			case PROLOG_START:
				return "PROLOG_START";
			case PROLOG_IDENTIFIER:
				return "PROLOG_IDENTIFIER";
			case ATTRIBUTE_VALUE:
				return "ATTRIBUTE_VALUE";
			case PROLOG_END:
				return "PROLOG_END";
			case TAG_START:
				return "TAG_START";
			case ELEMENT:
				return "ELEMENT";
			case ATTRIBUTE:
				return "ATTRIBUTE";
			case NULL_TAG_END:
				return "NULL_TAG_END";
			case TAG_CLOSE_START:
				return "TAG_CLOSE_START";
			case VALUE:
				return "VALUE";
			case TAG_END:
				return "TAG_END";
			default:
				return "UNKNOWN_TOKEN";
			}
		}
	}

	public XMLTokenizer(String filename) throws FileNotFoundException {
		this.filename = filename;
		this.emerge();
	}

	/**
	 * Assignment 5: Implementing the idiom Helper Method for Initializations.
	 */
	private void emerge() throws FileNotFoundException {
		reader = new BufferedReader(new FileReader(filename));
	}

	public void restart() {
		System.out.println("Restarting tokenizer");
		line_number = 0;
		line = null;
		try {
			this.reader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			System.out.println("Could not access file.");
			e.printStackTrace();
		}
	}

	public int getLineNumber() {
		return line_number;
	}

	public int getLineCharacter() {
		return index;
	}

	public XMLToken getNextToken() throws IOException {
		if (line == null) {
			line = reader.readLine();
			index = 0;
			line_number++;

			if (line == null)
				return new XMLToken(null, XMLToken.NULL); // Null Object pattern?

			// Reset matchers to use this new line.
			// TODO: Pattern Opportunity: Factory Method (significant repeated constructor
			// logic).
			if (prolog_start_matcher == null) {
				prolog_start_matcher = PROLOG_START.matcher(line);
				prolog_identifier_matcher = PROLOG_IDENTIFIER.matcher(line);
				attribute_value_matcher = ATTRIBUTE_VALUE.matcher(line);
				prolog_end_matcher = PROLOG_END.matcher(line);
				tag_start_matcher = TAG_START.matcher(line);
				element_matcher = ELEMENT.matcher(line);
				attribute_matcher = ATTRIBUTE.matcher(line);
				null_tag_end_matcher = NULL_TAG_END.matcher(line);
				tag_close_start_matcher = TAG_CLOSE_START.matcher(line);
				value_matcher = VALUE.matcher(line);
				tag_end_matcher = TAG_END.matcher(line);
				space_to_eol_matcher = SPACE_TO_EOL.matcher(line);
			} else
				update_matchers(0);
		}

		/**
		 * `.lookingAt() && .start() == 0` translates to "partial match from beginning".
		 * Consider simplify ensuing `.lookingAt() && .start() == 0` calls (perhaps with
		 * another function or a REGEX pattern with a start anchor.). Pattern
		 * Opportunity: Significant parallel logic. Consider Strategy, Template Method,
		 * and Flyweight.
		 */
		if (prolog_start_matcher.lookingAt() && prolog_start_matcher.start() == 0) {
			XMLToken token = new XMLToken(prolog_start_matcher.group(), XMLToken.PROLOG_START);
			inside_tag = true;
			update_matchers(prolog_start_matcher.end());
			return token;
		}

		if (prolog_end_matcher.lookingAt() && prolog_end_matcher.start() == 0) {
			XMLToken token = new XMLToken(prolog_end_matcher.group(), XMLToken.PROLOG_END);
			inside_tag = false;
			update_matchers(prolog_end_matcher.end());
			return token;
		}

		if (tag_close_start_matcher.lookingAt() && tag_close_start_matcher.start() == 0) {
			XMLToken token = new XMLToken(tag_close_start_matcher.group(), XMLToken.TAG_CLOSE_START);
			inside_tag = true;
			update_matchers(tag_close_start_matcher.end());
			return token;
		}

		if (tag_start_matcher.lookingAt() && tag_start_matcher.start() == 0) {
			XMLToken token = new XMLToken(tag_start_matcher.group(), XMLToken.TAG_START);
			inside_tag = true;
			update_matchers(tag_start_matcher.end());
			return token;
		}

		if (tag_end_matcher.lookingAt() && tag_end_matcher.start() == 0) {
			XMLToken token = new XMLToken(tag_end_matcher.group(), XMLToken.TAG_END);
			inside_tag = false;
			update_matchers(tag_end_matcher.end());
			return token;
		}

		if (null_tag_end_matcher.lookingAt() && null_tag_end_matcher.start() == 0) {
			XMLToken token = new XMLToken(null_tag_end_matcher.group(), XMLToken.NULL_TAG_END);
			inside_tag = false;
			update_matchers(null_tag_end_matcher.end());
			return token;
		}

		// Pattern Opportunity: Template Method or Strategy?
		if (inside_tag) {
			if (attribute_matcher.lookingAt() && attribute_matcher.start() == 0) {
				String val = attribute_matcher.group();
				val = val.replaceAll(" ", "");
				val = val.replaceAll("=", "");
				XMLToken token = new XMLToken(val, XMLToken.ATTRIBUTE);
				update_matchers(attribute_matcher.end());
				return token;
			}

			if (attribute_value_matcher.lookingAt() && attribute_value_matcher.start() == 0) {
				String val = attribute_value_matcher.group();
				val = val.replaceAll("\"", "");
				XMLToken token = new XMLToken(val, XMLToken.ATTRIBUTE_VALUE);
				update_matchers(attribute_value_matcher.end());
				return token;
			}

			if (prolog_identifier_matcher.lookingAt() && prolog_identifier_matcher.start() == 0) {
				XMLToken token = new XMLToken(prolog_identifier_matcher.group(), XMLToken.PROLOG_IDENTIFIER);
				update_matchers(prolog_identifier_matcher.end());
				return token;
			}

			if (element_matcher.lookingAt() && element_matcher.start() == 0) {
				XMLToken token = new XMLToken(element_matcher.group(), XMLToken.ELEMENT);
				update_matchers(element_matcher.end());
				return token;
			}
		} else if (value_matcher.lookingAt() && value_matcher.start() == 0) {
			XMLToken token = new XMLToken(value_matcher.group(), XMLToken.VALUE);
			update_matchers(value_matcher.end());
			return token;
		}

		if (space_to_eol_matcher.lookingAt() && space_to_eol_matcher.start() == 0) {
			update_matchers(space_to_eol_matcher.end());
			return getNextToken();
		}

		return new XMLToken("", XMLToken.NULL);
	}

	// Pattern Opportunity: This logic is similar to the start of `getNextToken()`.
	private void update_matchers(int index) {
		if (index != 0) {
			if (index >= line.length()) {
				line = null;
				return;
			}

			this.index += index;

			line = line.substring(index);
		}

		prolog_start_matcher = prolog_start_matcher.reset(line);
		prolog_identifier_matcher = prolog_identifier_matcher.reset(line);
		attribute_value_matcher = attribute_value_matcher.reset(line);
		prolog_end_matcher = prolog_end_matcher.reset(line);
		tag_start_matcher = tag_start_matcher.reset(line);
		element_matcher = element_matcher.reset(line);
		attribute_matcher = attribute_matcher.reset(line);
		null_tag_end_matcher = null_tag_end_matcher.reset(line);
		tag_close_start_matcher = tag_close_start_matcher.reset(line);
		value_matcher = value_matcher.reset(line);
		tag_end_matcher = tag_end_matcher.reset(line);
		space_to_eol_matcher = space_to_eol_matcher.reset(line);
	}

	public static void main(String args[]) {

		// Pattern Opportunity: Iterator.
		for (int i = 0; i < args.length; i++) {
			XMLTokenizer tokenizer = null;
			String filename = args[i];

			try {
				tokenizer = new XMLTokenizer(args[i]);
			} catch (FileNotFoundException e) {
				System.out.println("Unable to read file '" + filename + "'");
				continue;
			}

			XMLTokenizer.XMLToken token = null;

			System.out.println("File:  '" + filename + "'");

			do {
				try {
					token = tokenizer.getNextToken();
				} catch (IOException e) {
					System.out.println("IO Exception parsing file '" + filename + "':  " + e);
					e.printStackTrace();
				}

				System.out.println("\tLine " + tokenizer.getLineNumber() + ":  " + token + " = '"
						+ (token.getToken() == null ? "" : token.getToken()) + "'");
			} while (token.getTokenType() != XMLToken.NULL);

			// Assignment 3: Build a DOM tree using the Builder pattern.
			try {
				tokenizer = new XMLTokenizer(filename);
			} catch (FileNotFoundException e) {
				System.out.println("Unable to read file '" + filename + "'");
				continue;
			}
			ConcreteDOMBuilder builder = new ConcreteDOMBuilder(new ConcreteDOMFactory());

			// Disabling to facilitate debugging.
			// MockGUI mockGui = new MockGUI(); // Added for Assignment 7.
			// mockGui.attachSubject(builder);

			DOMDirector director = new DOMDirector(builder);
			try {
				director.construct(filename, 0);
			} catch (IOException e) {
				System.out.println("IO Exception building the DOM tree for file '" + filename + "':  " + e);
				e.printStackTrace();
			}
			NodeCompositeInterface domTree = builder.getResult();

			/**
			 * Assignment Module 13: Interpret expression input.
			 */
			Stack<Operation> operationStack = new Stack<>();
			NodeIterator iter = domTree.createIterator();
			while (iter.hasNext()) {
				edu.jhu.apl.patterns_class.dom.replacement.Node elm = iter.next();

				Operation parentOp = operationStack.empty() ? null : operationStack.peek();
				System.out.println("Elm:" + elm);

				if (elm.getClass().equals(Element.class)) {

					if (elm.getNodeName().equals("operation")) {
						String type = elm.getAttributes().getNamedItem("type").getNodeValue();
						if (type.equals("-"))
							operationStack.push(new UnaryMinus());
						else if (type.equals("+"))
							operationStack.push(new Addition());
						if(parentOp != null) parentOp.add(operationStack.peek());
					} else if (elm.getNodeName().equals("value")) {
						edu.jhu.apl.patterns_class.dom.replacement.Node text = elm.getChildNodes().item(0);
						Integer val = Integer.parseInt(text.getNodeValue());
						parentOp.add(new Constant(val));
					}

				}

			};
			// Evaluate root expression:
			Integer result = operationStack.get(0).evaluate();
			System.out.println("Evaluated result: " + result);

			// Assignment Module 11: Use Prototype to duplicate a DOM tree/subtree.
			// NodeCompositeInterface duplicateDOMTree = (NodeCompositeInterface)
			// domTree.duplicate(null);
			// System.out.println("Cloned DOM tree: " + duplicateDOMTree);

			// Assignment Module 9: Test CoR message handling:
			// System.out.println("\nTesting CoR:");
			// domTree.sendEvent("type1");
			// domTree.sendEvent("type2");

			System.out.println("Done creating DOM tree. Writing to TestTokenizer.txt.");
			try {
				XMLSerializer serializer = new XMLSerializer("TestTokenizer.txt");
				serializer.serializePretty(domTree);
				serializer.close();
			} catch (Exception e) {
				System.out.println("Could not write DOM tree to file.");
				e.printStackTrace();
			}

		}

	}

}
