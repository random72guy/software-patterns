package edu.jhu.apl.patterns_class;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import edu.jhu.apl.patterns_class.dom.ConcreteDOMBuilder;
import edu.jhu.apl.patterns_class.dom.ConcreteDOMFactory;
import edu.jhu.apl.patterns_class.dom.DOMDirector;
import edu.jhu.apl.patterns_class.dom.NodeCompositeInterface;
import edu.jhu.apl.patterns_class.support.commands.EditNodeValCommand;
import edu.jhu.apl.patterns_class.support.commands.ParseFileCommand;

/**
 * Created for Assignment Module 11. A command line interpreter to allow
 * commands to be specified by user at the command line.
 */
public class XMLParserCLI {
    private ParseFileCommand parseFileCommand = null;
    private EditNodeValCommand editNodeValCommand = null;
    private static Scanner scanner;

    public static void main(String args[]) {

        XMLParserCLI cli = new XMLParserCLI();
        try {

            System.out.println("Configuring the XMLParserCLI. Please wait...");

            String filename = "./Test1.xml";
            XMLTokenizer tokenizer = new XMLTokenizer(filename);
            ParseFileCommand pfCommand = new ParseFileCommand();
            pfCommand.setReceiver(tokenizer);
            cli.setParseFileCommand(pfCommand);

            ConcreteDOMBuilder builder = new ConcreteDOMBuilder(new ConcreteDOMFactory());
            DOMDirector director = new DOMDirector(builder);
			try {
				director.construct(filename, 0);
			} catch (IOException e) {
				System.out.println("IO Exception building the DOM tree for file '" + filename + "':  " + e);
				e.printStackTrace();
            }
            NodeCompositeInterface domTree = builder.getResult();
            EditNodeValCommand eaCommand = new EditNodeValCommand();
            eaCommand.setReceiver(domTree);
            cli.setEditRootValCommand(eaCommand);

            System.out.println("XMLParserCLI configuration complete. Starting...\n");

            cli.start();

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

    }

    public XMLParserCLI() {
    }

    /**
     * Starts the CLI input listener and command processor.
     */
    public void start() {
        System.out.println("Welcome to the XMLParser CLI.");
        this.executeHelp();

        scanner = new Scanner(System.in);
        String cmdStr;
        do {
            cmdStr = scanner.next();
            this.processInput(cmdStr);
        } while (!cmdStr.equals("exit"));
        scanner.close();
    }

    private void processInput(String input) {
        switch (input) {
            case "parse":
                this.executeParseCommand();
                break;
            case "edit-root-val":
                this.executeEditRootValCommand();
                break;
            case "exit":
                break;
            case "help":
            default: this.executeHelp();
        }
    }

    public void executeHelp() {
        System.out.println("HELP: Enter one of the following commands: help, edit-root-val, parse, exit");
    }

    public void setParseFileCommand(ParseFileCommand pfCommand) {
        this.parseFileCommand = pfCommand;
    }
    public void executeParseCommand() {
        try {
            ArrayList<XMLTokenizer.XMLToken> tokens = this.parseFileCommand.execute();
            System.out.println("Tokens: " + tokens);
        } catch (Exception e) {
            System.out.println("Error: Could not parse file. Error: " + e);
        }
    }

    public void setEditRootValCommand(EditNodeValCommand evCommand) {
        this.editNodeValCommand = evCommand;
    }
    public void executeEditRootValCommand() {
        System.out.println("Enter a new value:");
        String newVal = XMLParserCLI.scanner.next();
        NodeCompositeInterface res = this.editNodeValCommand.execute(newVal);
        System.out.println("Edited. Root node value is now: " + res.getNodeValue());
    }
}