package edu.jhu.apl.patterns_class.dom;

import edu.jhu.apl.patterns_class.dom.replacement.Node;
import edu.jhu.apl.patterns_class.support.DOMFlyweightExtrinsicState;

/**
 * Created for Assignment Module 13 Flyweight pattern.
 * This is a parent class for the `Text` and `Attr` Flyweights to handle any Node methods that would normally require extrinsic context.
 */
public abstract class NodeLeafFlyweight extends NodeLeaf {

    NodeLeafFlyweight(String name, short type) {
        super(name, type);
    }

    @Override
    public Node getParentNode() {
        return null;
    }

    @Override
    public Node getNextSibling() {
        return null;
    }

    @Override
    public Node getPreviousSibling() {
        return null;
    }

    /**
     * Position information (extrinsic state) is required to determine node equality.
     */
    @Override
    public boolean isSameNode(Node other) {
        return false;
    }

    public boolean isSameNode(Node other, DOMFlyweightExtrinsicState otherExtrinsicState, DOMFlyweightExtrinsicState ownExtrinsicState) {
        return isEqualNode(other) && other.getNodeValue().equals(this.getNodeValue()) && otherExtrinsicState.equals(ownExtrinsicState);
    }

    @Override
    public boolean isEqualNode(Node other) {
        return other.getNodeName().equals(this.getNodeName()) && other.getNodeType() == this.getNodeType();
    }


}
