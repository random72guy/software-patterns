package edu.jhu.apl.patterns_class.dom;

import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;
import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.DOMException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.EntityReference;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;
import org.w3c.dom.UserDataHandler;

import edu.jhu.apl.patterns_class.dom.replacement.Document;
import edu.jhu.apl.patterns_class.dom.replacement.Node;

/**
 * Created for Assignment 06 to adapt our Document (Adaptee) to the official W3C
 * Document interface (Target). This code assumes some W3C classes can be
 * successfully cast to their corresponding JHU class. Given the classes'
 * similarity, this seems likely; but it has not been tested.
 * https://docs.oracle.com/javase/7/docs/api/org/w3c/dom/Document.html
 */
public class DocumentToW3CDocumentAdapter implements org.w3c.dom.Document {
    private Document adaptee = null;

    DocumentToW3CDocumentAdapter(Document adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public String getNodeName() {
        return adaptee.getNodeName();
    }

    @Override
    public String getNodeValue() throws DOMException {
        return adaptee.getNodeValue();
    }

    @Override
    public void setNodeValue(String nodeValue) throws DOMException {
        adaptee.setNodeValue(nodeValue);
    }

    @Override
    public short getNodeType() {
        return adaptee.getNodeType();
    }

    @Override
    public org.w3c.dom.Node getParentNode() {
        return new NodeToW3CNodeAdapter(adaptee.getParentNode());
    }

    @Override
    public NodeList getChildNodes() {
        return (NodeList) adaptee.getChildNodes();
    }

    @Override
    public org.w3c.dom.Node getFirstChild() {
        return new NodeToW3CNodeAdapter(adaptee.getFirstChild());
    }

    @Override
    public org.w3c.dom.Node getLastChild() {
        return new NodeToW3CNodeAdapter(adaptee.getLastChild());
    }

    @Override
    public org.w3c.dom.Node getPreviousSibling() {
        return new NodeToW3CNodeAdapter(adaptee.getPreviousSibling());
    }

    @Override
    public org.w3c.dom.Node getNextSibling() {
        return new NodeToW3CNodeAdapter(adaptee.getNextSibling());
    }

    @Override
    public NamedNodeMap getAttributes() {
        return (NamedNodeMap) adaptee.getAttributes();
    }

    @Override
    public org.w3c.dom.Document getOwnerDocument() {
        return new DocumentToW3CDocumentAdapter(adaptee.getOwnerDocument());
    }

    @Override
    public org.w3c.dom.Node insertBefore(org.w3c.dom.Node newChild, org.w3c.dom.Node refChild) throws DOMException {
        return new NodeToW3CNodeAdapter(adaptee.insertBefore((Node) newChild, (Node) refChild));
    }

    @Override
    public org.w3c.dom.Node replaceChild(org.w3c.dom.Node newChild, org.w3c.dom.Node oldChild) throws DOMException {
        return new NodeToW3CNodeAdapter(adaptee.replaceChild((Node) newChild, (Node) oldChild));
    }

    @Override
    public org.w3c.dom.Node removeChild(org.w3c.dom.Node oldChild) throws DOMException {
        return new NodeToW3CNodeAdapter(adaptee.removeChild((Node) oldChild));
    }

    @Override
    public org.w3c.dom.Node appendChild(org.w3c.dom.Node newChild) throws DOMException {
        return new NodeToW3CNodeAdapter(adaptee.appendChild((Node) newChild));
    }

    @Override
    public boolean hasChildNodes() {
        return adaptee.hasChildNodes();
    }

    @Override
    public org.w3c.dom.Node cloneNode(boolean deep) {
        return new NodeToW3CNodeAdapter(adaptee.cloneNode(deep));
    }

    @Override
    public void normalize() {
        adaptee.normalize();
    }

    @Override
    public boolean isSupported(String feature, String version) {
        return adaptee.isSupported(feature, version);
    }

    @Override
    public String getNamespaceURI() {
        return adaptee.getNamespaceURI();
    }

    @Override
    public String getPrefix() {
        return adaptee.getPrefix();
    }

    @Override
    public void setPrefix(String prefix) throws DOMException {
        adaptee.setPrefix(prefix);
    }

    @Override
    public String getLocalName() {
        return adaptee.getLocalName();
    }

    @Override
    public boolean hasAttributes() {
        return adaptee.hasAttributes();
    }

    @Override
    public String getBaseURI() {
        return adaptee.getBaseURI();
    }

    @Override
    public short compareDocumentPosition(org.w3c.dom.Node other) throws DOMException {
        return adaptee.compareDocumentPosition((Node) other);
    }

    @Override
    public String getTextContent() throws DOMException {
        return adaptee.getTextContent();
    }

    @Override
    public void setTextContent(String textContent) throws DOMException {
        adaptee.setTextContent(textContent);
        ;
    }

    @Override
    public boolean isSameNode(org.w3c.dom.Node other) {
        return adaptee.isSameNode((Node) other);
    }

    @Override
    public String lookupPrefix(String namespaceURI) {
        return adaptee.lookupPrefix(namespaceURI);
    }

    @Override
    public boolean isDefaultNamespace(String namespaceURI) {
        return adaptee.isDefaultNamespace(namespaceURI);
    }

    @Override
    public String lookupNamespaceURI(String prefix) {
        return adaptee.lookupNamespaceURI(prefix);
    }

    @Override
    public boolean isEqualNode(org.w3c.dom.Node arg) {
        return adaptee.isEqualNode((Node) arg);
    }

    @Override
    public Object getFeature(String feature, String version) {
        return adaptee.getFeature(feature, version);
    }

    @Override
    public Object setUserData(String key, Object data, UserDataHandler handler) {
        return adaptee.setUserData(key, data, handler);
    }

    @Override
    public Object getUserData(String key) {
        return adaptee.getUserData(key);
    }

    @Override
    public DocumentType getDoctype() {
        return adaptee.getDoctype();
    }

    @Override
    public DOMImplementation getImplementation() {
        return adaptee.getImplementation();
    }

    @Override
    public Element getDocumentElement() {
        return (Element) adaptee.getDocumentElement();
    }

    @Override
    public Element createElement(String tagName) throws DOMException {
        return (Element) adaptee.createElement(tagName);
    }

    @Override
    public DocumentFragment createDocumentFragment() {
        return adaptee.createDocumentFragment();
    }

    @Override
    public Text createTextNode(String data) {
        return (Text) adaptee.createTextNode(data);
    }

    @Override
    public Comment createComment(String data) {
        return (Comment) adaptee.createComment(data);
    }

    @Override
    public CDATASection createCDATASection(String data) throws DOMException {
        return adaptee.createCDATASection(data);
    }

    @Override
    public ProcessingInstruction createProcessingInstruction(String target, String data) throws DOMException {
        return adaptee.createProcessingInstruction(target, data);
    }

    @Override
    public Attr createAttribute(String name) throws DOMException {
        return (Attr) adaptee.createAttribute(name);
    }

    @Override
    public EntityReference createEntityReference(String name) throws DOMException {
        return adaptee.createEntityReference(name);
    }

    @Override
    public NodeList getElementsByTagName(String tagname) {
        return null; // No corresponding function in our implementation.
    }

    @Override
    public org.w3c.dom.Node importNode(org.w3c.dom.Node importedNode, boolean deep) throws DOMException {
        return new NodeToW3CNodeAdapter(adaptee.importNode((Node) importedNode, deep));
    }

    @Override
    public Element createElementNS(String namespaceURI, String qualifiedName) throws DOMException {
        return (Element) adaptee.createElementNS(namespaceURI, qualifiedName);
    }

    @Override
    public Attr createAttributeNS(String namespaceURI, String qualifiedName) throws DOMException {
        return (Attr) adaptee.createAttributeNS(namespaceURI, qualifiedName);
    }

    @Override
    public NodeList getElementsByTagNameNS(String namespaceURI, String localName) {
        return (NodeList) adaptee.getElementsByTagNameNS(namespaceURI, localName);
    }

    @Override
    public Element getElementById(String elementId) {
        return null; // Not implemented in JHU Node.
    }

    @Override
    public String getInputEncoding() {
        return adaptee.getInputEncoding();
    }

    @Override
    public String getXmlEncoding() {
        return adaptee.getXmlEncoding();
    }

    @Override
    public boolean getXmlStandalone() {
        return adaptee.getXmlStandalone();
    }

    @Override
    public void setXmlStandalone(boolean xmlStandalone) throws DOMException {
        adaptee.setXmlStandalone(xmlStandalone);
    }

    @Override
    public String getXmlVersion() {
        return adaptee.getXmlVersion();
    }

    @Override
    public void setXmlVersion(String xmlVersion) throws DOMException {
        adaptee.setXmlVersion(xmlVersion);
    }

    @Override
    public boolean getStrictErrorChecking() {
        return adaptee.getStrictErrorChecking();
    }

    @Override
    public void setStrictErrorChecking(boolean strictErrorChecking) {
        adaptee.setStrictErrorChecking(strictErrorChecking);
    }

    @Override
    public String getDocumentURI() {
        return adaptee.getDocumentURI();
    }

    @Override
    public void setDocumentURI(String documentURI) {
        adaptee.setDocumentURI(documentURI);
    }

    @Override
    public org.w3c.dom.Node adoptNode(org.w3c.dom.Node source) throws DOMException {
        return new NodeToW3CNodeAdapter(adaptee.adoptNode((Node) source));
    }

    @Override
    public DOMConfiguration getDomConfig() {
        return adaptee.getDomConfig();
    }

    @Override
    public void normalizeDocument() {
        adaptee.normalizeDocument();
    }

    @Override
    public org.w3c.dom.Node renameNode(org.w3c.dom.Node n, String namespaceURI, String qualifiedName)
            throws DOMException {
        return new NodeToW3CNodeAdapter(adaptee.renameNode((Node) n, namespaceURI, qualifiedName));
    }

}