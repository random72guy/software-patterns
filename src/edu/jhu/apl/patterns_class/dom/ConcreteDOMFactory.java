package edu.jhu.apl.patterns_class.dom;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created for Assignment 3. Implements a DOM factory for creating DOM nodes.
 * Technically monostate (Assignment 4), as it has no non-static member methods.
 * For Assignment Module 13, added flyweight pool methods so that this implements FlyweightFactory in the Flyweight pattern.
 */
public class ConcreteDOMFactory extends AbstractDOMFactory {

	/**
	 * Flyweight pools added for Assignment Module 13.
	 * Implemented as a List so that the lookup can leverage `Node.isEqualNode()`.
	 */
	private List<NodeLeafFlyweight> flyweightPool = new LinkedList<>();

	public ConcreteDOMFactory() {
	}

	@Override
	public Document createDocument() {
		return new Document();
	}

	@Override
	public Attr createAttr(String name, String value, Document document) {
		Attr flyweight = this.getAttrFlyweight(name, value);
		flyweight.document = document;
		return flyweight;
	}


	@Override
	public ElementProxy createElementProxy(String tagname, Document document, Integer tokenIndex, String filename) {
		return new ElementProxy(tagname, document, tokenIndex, filename);
	}

	@Override
	public ElementInterface createElement(String tagname, Document document) {
		return new Element(tagname, document);
	}

	@Override
	public Text createText(String value, Document document) {
		Text flyweight = this.getTextFlyweight(value);
		flyweight.document = document;
		return flyweight;
	}


	/**
	 * Added for Assignment Module 13.
	 */
	private Text getTextFlyweight(String value) {
		Text nodeToFind = new Text(value, null);
		Text found = (Text) findInPool(nodeToFind);
		if (found != null) return found;
		else return nodeToFind;
	}

	/**
	 * Added for Assignment Module 13.
	 */
	private Attr getAttrFlyweight(String name, String value) {
		Attr nodeToFind = new Attr(name, null);
		nodeToFind.setNodeValue(value);
		Attr found = (Attr) findInPool(nodeToFind);
		if (found != null) return found;
		else return nodeToFind;
	}

	/**
	 * Added for Assignment Module 13.
	 */
	private NodeLeafFlyweight findInPool(Node nodeToFind) {
		List<NodeLeafFlyweight> matches = this.flyweightPool.stream()
			.filter( node -> node.isEqualNode(nodeToFind) )
			.collect(Collectors.toList());
		if (!matches.isEmpty()) return matches.get(0);
		else return null;
	}
}
