package edu.jhu.apl.patterns_class.dom;

import org.w3c.dom.DOMException;
import edu.jhu.apl.patterns_class.dom.replacement.Node;

public interface NodeCompositeInterface extends Node {

    public Node appendProxyChild(ElementProxy newChild);

    public Node appendChild(Node newChild) throws DOMException;

    public NodeList getChildNodes();

    public Node getFirstChild();

    public Node getLastChild();

    public boolean hasChildNodes();

    public Node removeChild(Node oldChild) throws DOMException;

    public Node replaceChild(Node newChild, Node oldChild) throws DOMException;

    public Node insertBefore(Node newChild, Node refChild) throws DOMException;

    // Added for Assignment Module 9 Chain of Responsibilty.
    public void sendEvent(String eventType);

    @Override
    public NodeIterator createIterator() throws DOMException;
}