package edu.jhu.apl.patterns_class.dom;

import org.w3c.dom.DOMException;

import edu.jhu.apl.patterns_class.support.Subject;

/**
 * Created for Assignment 3. Defines the interface for a DOMBuilder.
 */
public abstract class AbstractDOMBuilder extends Subject<AbstractDOMBuilder> {
	public abstract NodeCompositeInterface getResult();

	public abstract Document buildDocument() throws DOMException;

	public abstract ElementInterface buildElement(NodeCompositeInterface parent, String tagName);

	public abstract ElementInterface buildProxyElement(NodeCompositeInterface parent, String tagName, Integer tokenizerIndex, String filename);


	public abstract Attr buildAttr(NodeCompositeInterface parent, String name, String value);

	public abstract Text buildText(ElementInterface parent, String text);
}