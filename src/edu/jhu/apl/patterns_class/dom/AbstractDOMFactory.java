package edu.jhu.apl.patterns_class.dom;

import edu.jhu.apl.patterns_class.XMLTokenizer;

/**
 * Created for Assignment 3. Defines a DOM factory.
 */
public abstract class AbstractDOMFactory {
	public AbstractDOMFactory() {
	}

	public abstract Document createDocument();

	public abstract ElementProxy createElementProxy(String tagname, Document document, Integer tokenIndex, String filename);

	public abstract ElementInterface createElement(String tagname, Document document);

	public abstract Attr createAttr(String name, String value, Document document);

	public abstract Text createText(String value, Document document);
}
