package edu.jhu.apl.patterns_class.dom;
import java.util.NoSuchElementException;
import org.w3c.dom.DOMException;
import edu.jhu.apl.patterns_class.dom.NodeList;

/**
 * Created for Assignment 2.
 * NodeLeaf represents a Leaf in the Composite Pattern. Methods requiring children are overridden, as a Leaf has no children.
 */
public abstract class NodeLeaf extends Node {

    NodeLeaf(String name, short type) {
        super(name, type);
    }

    public edu.jhu.apl.patterns_class.dom.replacement.Node appendChild(
            edu.jhu.apl.patterns_class.dom.replacement.Node newChild) throws DOMException {
        throw new DOMException((short) 3, "Cannot add an child to a leaf.");
    }

    public NodeList getChildNodes() {
        return new NodeList();
    }

    public edu.jhu.apl.patterns_class.dom.replacement.Node getFirstChild() {
        throw new NoSuchElementException();
    }

    public edu.jhu.apl.patterns_class.dom.replacement.Node getLastChild() {
        throw new NoSuchElementException();
    }

    public boolean hasChildNodes() {
        return false;
    }

    public edu.jhu.apl.patterns_class.dom.replacement.Node removeChild(
            edu.jhu.apl.patterns_class.dom.replacement.Node oldChild) throws DOMException {
        throw new DOMException((short) 3, "Cannot remove a child from a leaf.");
    }

    public edu.jhu.apl.patterns_class.dom.replacement.Node replaceChild(
            edu.jhu.apl.patterns_class.dom.replacement.Node newChild,
            edu.jhu.apl.patterns_class.dom.replacement.Node oldChild) throws DOMException {
        throw new DOMException((short) 3, "A leaf has no children to replace.");
    }

    public edu.jhu.apl.patterns_class.dom.replacement.Node insertBefore(
            edu.jhu.apl.patterns_class.dom.replacement.Node newChild,
            edu.jhu.apl.patterns_class.dom.replacement.Node refChild) throws DOMException {
        throw new DOMException((short) 3, "A leaf cannot have children.");
    }

    @Override
    public NodeIterator createIterator() throws DOMException {
        return new NodeIterator(this);
    }

}
