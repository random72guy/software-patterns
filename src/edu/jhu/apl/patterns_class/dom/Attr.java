package edu.jhu.apl.patterns_class.dom;

import edu.jhu.apl.patterns_class.dom.replacement.Node;

public class Attr extends NodeLeafFlyweight implements edu.jhu.apl.patterns_class.dom.replacement.Attr {
	public Attr(String tagName, Document document) {
		super(tagName, org.w3c.dom.Node.ATTRIBUTE_NODE);
		this.document = document;
	}

	public Attr(String tagName, String value, Document document) {
		super(tagName, org.w3c.dom.Node.ATTRIBUTE_NODE);
		this.document = document;
		setValue(value);
	}

	// Added for Assignment Module 11.
	@Override
	public Node duplicate(edu.jhu.apl.patterns_class.dom.replacement.Document document) {
		Attr dup = new Attr(this.getName(), (Document) document);
		dup.setNodeValue(this.getNodeValue());
		dup.setPrefix(this.getPrefix());
		dup.setTextContent(this.getTextContent());
		dup.setValue(this.getValue());
		return dup;
	}

	//
	// Implemented Attr members.
	//
	public String getName() {
		return getNodeName();
	}

	public String getValue() {
		return getNodeValue();
	}

	public void setValue(String value) {
		// TODO: Check for readonly status. NO_MODIFICATION_ALLOWED_ERR

		setNodeValue(value);
	}

	public edu.jhu.apl.patterns_class.dom.replacement.Element getOwnerElement() {
		return null;
	}

	//
	// Unimplemented Attr members.
	//
	public boolean getSpecified() {
		return true;
	}

	public boolean isId() {
		return false;
	}

	public org.w3c.dom.TypeInfo getSchemaTypeInfo() {
		return null;
	}
}
