package edu.jhu.apl.patterns_class.dom;

import org.w3c.dom.DOMException;
import edu.jhu.apl.patterns_class.dom.replacement.Node;

// Assignment 2: Created `NodeComposite` represent Composite class in the Composite pattern.
/**
 * Created for Assignment 2. NodeComposite represents a Composite in the
 * Composite Pattern. Methods requiring children may be overridden to provide a
 * default implementation, and may be overridden by children classes when
 * appropriate. Child-related method implementation was moved from `Node` to
 * `NodeComposite`, as the logic really only pertains to Composites, not Leaves,
 * allowing `Composite` to focus on logic pertaining only to Nodes with
 * children, while `Node` can handle logic applicable to all nodes.
 */
public abstract class NodeComposite extends edu.jhu.apl.patterns_class.dom.Node implements NodeCompositeInterface {

	private NodeList nodes = null;

	NodeComposite(String name, short type) {
		super(name, type);
		nodes = new NodeList();
	}

	/**
	 * Added for Assignment Module 9 Chain of Responsibilty.
	 * Handles an event if it can. Otherwise sends to children.
	 */
	public void sendEvent(String eventType) {

		// Check if we handle it
		boolean handle = false;
		if (this.hasAttributes()) {
			Attr messageAttr = (Attr) this.getAttributes().getNamedItem("message");
			handle = messageAttr != null && messageAttr.getValue().equals(eventType);
		}

		if (handle)
			System.out.println("Node " + this + " handling event " + eventType + ".");
		else {
			System.out.println("Node " + this + " not handling event " + eventType + ". Sending to children.");
			// Send to children, if any.
			this.getChildNodes().forEach(node -> {
				((NodeComposite) node).sendEvent(eventType);
			});
		}

	};

	/**
	 * Adds a child without performing actions on the proxy that would force a load
	 * of the real element.
	 */
	// public Node appendProxyChild(ElementProxy newChild) {
	// nodes.addLast(newChild);
	// return newChild;
	// }

	public Node appendChild(Node newChild) throws org.w3c.dom.DOMException {
		// TODO: Do readonly checks on this node and current parent of newChild.
		// NO_MODIFICATION_ALLOWED_ERR
		// TODO: Exclude child types not permitted for this element here.
		// HIERARCHY_REQUEST_ERR

		if (newChild.getOwnerDocument() != getOwnerDocument())
			throw new org.w3c.dom.DOMException(org.w3c.dom.DOMException.WRONG_DOCUMENT_ERR,
					"New Child is not a part of this document.");

		if (newChild.getParentNode() != null)
			newChild.getParentNode().removeChild(newChild);

		nodes.addLast(newChild);
		newChild.setParent(this);

		return newChild;
	}

	public NodeList getChildNodes() {
		return nodes;
	}

	public Node getFirstChild() throws DOMException {
		if (nodes.isEmpty())
			throw new org.w3c.dom.DOMException(org.w3c.dom.DOMException.NOT_FOUND_ERR, "No child nodes exist.");

		return nodes.getFirst();
	}

	public Node getLastChild() throws DOMException {
		if (nodes.isEmpty())
			throw new org.w3c.dom.DOMException(org.w3c.dom.DOMException.NOT_FOUND_ERR, "No child nodes exist.");
		return nodes.getLast();
	}

	public boolean hasChildNodes() {
		return nodes.size() > 0;
	}

	public Node removeChild(Node oldChild) throws DOMException {
		// TODO: Do readonly checks on this node. NO_MODIFICATION_ALLOWED_ERR

		int index = nodes.indexOf(oldChild);

		if (index == -1)
			throw new org.w3c.dom.DOMException(org.w3c.dom.DOMException.NOT_FOUND_ERR,
					"Old Child is not a child of this node.");

		((Node) nodes.get(index)).setParent(null);
		nodes.remove(index);

		return oldChild;
	}

	public Node replaceChild(Node newChild, Node oldChild) throws DOMException {
		// TODO: Do readonly checks on this node and current parent of newChild.
		// NO_MODIFICATION_ALLOWED_ERR
		// TODO: Exclude child types not permitted for this element here.
		// HIERARCHY_REQUEST_ERR

		if (newChild.getOwnerDocument() != getOwnerDocument())
			throw new org.w3c.dom.DOMException(org.w3c.dom.DOMException.WRONG_DOCUMENT_ERR,
					"New Child is not a part of this document.");

		if (newChild.getParentNode() != null)
			newChild.getParentNode().removeChild(newChild);

		int index = nodes.indexOf(oldChild);

		if (index == -1)
			throw new org.w3c.dom.DOMException(org.w3c.dom.DOMException.NOT_FOUND_ERR,
					"Old Child is not a child of this node.");

		nodes.add(index, newChild);
		((Node) newChild).setParent(this);
		((Node) nodes.get(index + 1)).setParent(null);
		nodes.remove(index + 1);

		return oldChild;
	}

	public Node insertBefore(Node newChild, Node refChild) throws org.w3c.dom.DOMException {
		// TODO: Do readonly checks on this node and current parent of newChild.
		// NO_MODIFICATION_ALLOWED_ERR
		// TODO: Exclude child types not permitted for this element here.
		// HIERARCHY_REQUEST_ERR

		if (newChild.getOwnerDocument() != getOwnerDocument())
			throw new org.w3c.dom.DOMException(org.w3c.dom.DOMException.WRONG_DOCUMENT_ERR,
					"New Child is not a part of this document.");

		if (newChild.getParentNode() != null)
			newChild.getParentNode().removeChild(newChild);

		if (refChild == null) {
			nodes.addLast(newChild);
			((Node) newChild).setParent(this);
			return newChild;
		}

		int index = nodes.indexOf(refChild);

		if (index == -1)
			throw new org.w3c.dom.DOMException(org.w3c.dom.DOMException.NOT_FOUND_ERR,
					"Reference Child is not a child of this node.");

		nodes.add(index, newChild);
		((Node) newChild).setParent(this);

		return newChild;
	}

	@Override
	public NodeIterator createIterator() throws DOMException {
		return new NodeIterator(this);
	}

}
