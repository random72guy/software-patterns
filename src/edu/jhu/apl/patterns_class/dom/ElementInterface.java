package edu.jhu.apl.patterns_class.dom;

import edu.jhu.apl.patterns_class.dom.replacement.Element;

public interface ElementInterface extends NodeCompositeInterface, Element {
    
}