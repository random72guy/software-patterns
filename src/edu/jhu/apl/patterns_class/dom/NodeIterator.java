package edu.jhu.apl.patterns_class.dom;
import edu.jhu.apl.patterns_class.dom.replacement.Node;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Performs a Preorder traversal of the nodes, starting with the supplied root node.
 * Created for Assignment 2.
 * Implements the `ConcreteIterator` from the Iterator pattern. Java's `Iterator` takes the place of `Iterator` from the pattern, and references the `Aggregate` `Node`. This, our ConcreteIterator, references the `ConcreteAggregate` `node`.
 * Note: While the original code in `XMLSerializer.java` uses `ListIterator`, it does not appear to fully utilize the interface; for simplicity's sake, `Iterator` is being used as a substitute.
 * Note: This is a slightly modified implementation of the Iterator pattern. Instead of directly referencing a concrete `Node`, maintaining a cursor, and holding all the traversal logic, this class leverages the Java Utils Iterator, which does all this already. This class mainly converts the Node hierarchy into a form the iterator can work with. While the traversal logic could be placed here, it doesn't seem necessary. This class still provides value by abstracting traversal logic from the Serializer, simplifying Serializer's code and allowing multiple classes to leverage the algorithm.
 */
public class NodeIterator implements Iterator<Node> {
    private List<Node> list = new LinkedList<Node>();
    private Iterator<Node> iterator = null;

    public NodeIterator(Node node) {
        this.list = this.createPreorderList(node, this.list);
        this.iterator = this.list.iterator();
    }

    @Override
    public boolean hasNext() {
        return this.iterator.hasNext();
    }

    @Override
    public Node next() {
        return this.iterator.next();
    }

    private List<Node> createPreorderList(Node node, List<Node> list) {
        list.add(node);
        if (node.hasChildNodes()) {
            Node curNode = node.getFirstChild();
            while (curNode != null) {
                this.createPreorderList(curNode, list);
                curNode = curNode.getNextSibling();
            }
        }
        return list;
    }

}