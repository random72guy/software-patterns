package edu.jhu.apl.patterns_class.dom;

import org.w3c.dom.DOMException;

/**
 * Created for Assignment 3. Implementation of a DOMBuilder. Assembles a DOM
 * tree by adding child nodes and attributes to parents. Can create nodes using
 * the a DOM Factory.
 */
public class ConcreteDOMBuilder extends AbstractDOMBuilder {

	private Document rootDocument = new Document();
	private AbstractDOMFactory factory;

	public ConcreteDOMBuilder(AbstractDOMFactory factory) {
		this.factory = factory;
	}

	public NodeCompositeInterface getResult() {
		return this.rootDocument;
	}

	@Override
	public Document buildDocument() throws DOMException {
		Document document = this.factory.createDocument();
		this.rootDocument = document;
		this.notifyObservers("buildDocument"); // Added for Assignment 7.
		return document;
	}

    @Override
	public ElementInterface buildElement(NodeCompositeInterface parent, String tagName) throws DOMException {
		ElementInterface newNode = this.factory.createElement(tagName, this.rootDocument);
		if (parent != null)
			parent.appendChild(newNode);
		this.notifyObservers("buildElement"); // Added for Assignment 7.
		return newNode;
	}

	@Override
	public ElementInterface buildProxyElement(NodeCompositeInterface parent, String tagName, Integer tokenizerIndex,
			String filename) throws DOMException {
		ElementProxy newNode = this.factory.createElementProxy(tagName, this.rootDocument, tokenizerIndex, filename);
		if (parent != null)
			parent.appendProxyChild(newNode);
		this.notifyObservers("buildProxyElement"); // Added for Assignment 7.
		return newNode;
	}

	@Override
	public Attr buildAttr(NodeCompositeInterface parent, String name, String value) {
		Attr newNode = this.factory.createAttr(name, value, this.rootDocument);
		if (parent instanceof Element)
			((Element) parent).setAttributeNode(newNode);
		this.notifyObservers("buildAttr"); // Added for Assignment 7.
		return newNode;
	}

	@Override
	public Text buildText(ElementInterface parent, String text) {
		Text newNode = this.factory.createText(text, this.rootDocument);
		parent.setTextContent(text); // TODO: Verify this is appropriate.
		parent.appendChild(newNode);
		this.notifyObservers("buildText"); // Added for Assignment 7.
		return newNode;
	}

}
