package edu.jhu.apl.patterns_class.dom;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.dom.UserDataHandler;

import edu.jhu.apl.patterns_class.dom.replacement.Node;

/**
 * Created for Assignment 06 to adapt our Node (Adaptee) to the official W3C
 * Node interface (Target). This code assumes the W3C Node can be successfully
 * cast to a JHU Node. Given the classes' similarity, this seems likely; but it
 * is untested. https://docs.oracle.com/javase/7/docs/api/org/w3c/dom/Node.html
 */
public class NodeToW3CNodeAdapter implements org.w3c.dom.Node {
    private Node adaptee = null;

    NodeToW3CNodeAdapter(Node adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public String getNodeName() {
        return adaptee.getNodeName();
    }

    @Override
    public String getNodeValue() throws DOMException {
        return adaptee.getNodeValue();
    }

    @Override
    public void setNodeValue(String nodeValue) throws DOMException {
        adaptee.setNodeValue(nodeValue);
    }

    @Override
    public short getNodeType() {
        return adaptee.getNodeType();
    }

    @Override
    public org.w3c.dom.Node getParentNode() {
        return new NodeToW3CNodeAdapter(adaptee.getParentNode());
    }

    @Override
    public NodeList getChildNodes() {
        return (NodeList) adaptee.getChildNodes();
    }

    @Override
    public org.w3c.dom.Node getFirstChild() {
        return new NodeToW3CNodeAdapter(adaptee.getFirstChild());
    }

    @Override
    public org.w3c.dom.Node getLastChild() {
        return new NodeToW3CNodeAdapter(adaptee.getLastChild());
    }

    @Override
    public org.w3c.dom.Node getPreviousSibling() {
        return new NodeToW3CNodeAdapter(adaptee.getPreviousSibling());
    }

    @Override
    public org.w3c.dom.Node getNextSibling() {
        return new NodeToW3CNodeAdapter(adaptee.getNextSibling());
    }

    @Override
    public NamedNodeMap getAttributes() {
        return (NamedNodeMap) adaptee.getAttributes();
    }

    @Override
    public Document getOwnerDocument() {
        return new DocumentToW3CDocumentAdapter(adaptee.getOwnerDocument());
    }

    @Override
    public org.w3c.dom.Node insertBefore(org.w3c.dom.Node newChild, org.w3c.dom.Node refChild) throws DOMException {
        return new NodeToW3CNodeAdapter(adaptee.insertBefore((Node) newChild, (Node) refChild));
    }

    @Override
    public org.w3c.dom.Node replaceChild(org.w3c.dom.Node newChild, org.w3c.dom.Node oldChild) throws DOMException {
        return new NodeToW3CNodeAdapter(adaptee.replaceChild((Node) newChild, (Node) oldChild));
    }

    @Override
    public org.w3c.dom.Node removeChild(org.w3c.dom.Node oldChild) throws DOMException {
        return new NodeToW3CNodeAdapter(adaptee.removeChild((Node) oldChild));
    }

    @Override
    public org.w3c.dom.Node appendChild(org.w3c.dom.Node newChild) throws DOMException {
        return new NodeToW3CNodeAdapter(this.adaptee.appendChild((Node) newChild));
    }

    @Override
    public boolean hasChildNodes() {
        return adaptee.hasChildNodes();
    }

    @Override
    public org.w3c.dom.Node cloneNode(boolean deep) {
        return new NodeToW3CNodeAdapter(adaptee.cloneNode(deep));
    }

    @Override
    public void normalize() {
        adaptee.normalize();
    }

    @Override
    public boolean isSupported(String feature, String version) {
        return adaptee.isSupported(feature, version);
    }

    @Override
    public String getNamespaceURI() {
        return adaptee.getNamespaceURI();
    }

    @Override
    public String getPrefix() {
        return adaptee.getNamespaceURI();
    }

    @Override
    public void setPrefix(String prefix) throws DOMException {
        adaptee.setPrefix(prefix);
    }

    @Override
    public String getLocalName() {
        return adaptee.getLocalName();
    }

    @Override
    public boolean hasAttributes() {
        return adaptee.hasAttributes();
    }

    @Override
    public String getBaseURI() {
        return adaptee.getBaseURI();
    }

    @Override
    public short compareDocumentPosition(org.w3c.dom.Node other) throws DOMException {
        return adaptee.compareDocumentPosition((Node) other);
    }

    @Override
    public String getTextContent() throws DOMException {
        return adaptee.getTextContent();
    }

    @Override
    public void setTextContent(String textContent) throws DOMException {
        adaptee.setTextContent(textContent);
    }

    @Override
    public boolean isSameNode(org.w3c.dom.Node other) {
        return adaptee.isSameNode((Node) other);
    }

    @Override
    public String lookupPrefix(String namespaceURI) {
        return adaptee.lookupPrefix(namespaceURI);
    }

    @Override
    public boolean isDefaultNamespace(String namespaceURI) {
        return adaptee.isDefaultNamespace(namespaceURI);
    }

    @Override
    public String lookupNamespaceURI(String prefix) {
        return adaptee.lookupNamespaceURI(prefix);
    }

    @Override
    public boolean isEqualNode(org.w3c.dom.Node arg) {
        return adaptee.isEqualNode((Node) arg);
    }

    @Override
    public Object getFeature(String feature, String version) {
        return adaptee.getFeature(feature, version);
    }

    @Override
    public Object setUserData(String key, Object data, UserDataHandler handler) {
        return adaptee.setUserData(key, data, handler);
    }

    @Override
    public Object getUserData(String key) {
        return adaptee.getUserData(key);
    }

}