package edu.jhu.apl.patterns_class.dom;

import java.io.IOException;

import edu.jhu.apl.patterns_class.XMLTokenizer;
import edu.jhu.apl.patterns_class.support.state.BuildContext;
import edu.jhu.apl.patterns_class.support.state.DoneBuildState;

/**
 * Assembles a DOM tree using a DOMBuilder. Created for Assignment 3. For
 * assignment 4, adjusted to build virtual Elements for each sub-level.
 * For Assignment Module 11 State pattern, refactored to use BuildContext.
 */
public class DOMDirector {

	protected AbstractDOMBuilder builder;

	public DOMDirector(AbstractDOMBuilder builder) {
		this.builder = builder;
	}

	public void construct(String filename, Integer startingLineNumber) throws IOException {

		System.out.println("Constructing DOM tree from line " + startingLineNumber);

		XMLTokenizer tokenizer = new XMLTokenizer(filename);

		// Added for Assignemnt Module 12 State pattern.
		BuildContext buildContext = new BuildContext(tokenizer, builder, filename);
		do {
			buildContext.build(startingLineNumber);
		}
		while ( !(buildContext.state instanceof DoneBuildState));

	}
}