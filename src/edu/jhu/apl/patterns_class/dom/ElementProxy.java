package edu.jhu.apl.patterns_class.dom;

import java.io.IOException;

import org.w3c.dom.DOMException;
import org.w3c.dom.TypeInfo;
import org.w3c.dom.UserDataHandler;

import edu.jhu.apl.patterns_class.dom.Document;
import edu.jhu.apl.patterns_class.dom.replacement.Attr;
import edu.jhu.apl.patterns_class.dom.replacement.NamedNodeMap;
import edu.jhu.apl.patterns_class.dom.replacement.Node;
import edu.jhu.apl.patterns_class.dom.replacement.NodeList;
import edu.jhu.apl.patterns_class.support.NodeVisitor;

/**
 * Virtual Proxy for Element so that its content can be loaded later by the DOM
 * Builder.
 * Implemented for Assignment 4.
 */
public class ElementProxy implements ElementInterface {
	private String tagname = null;
	private Document document = null;
	private Integer tokenIndex = null;
	private String filename = null;
	private ElementInterface element = null;

	public ElementProxy(String tagname, Document document, Integer tokenIndex, String filename) {
		this.tagname = tagname;
		this.document = document;
		this.tokenIndex = tokenIndex;
		this.filename = filename;
	}

	/**
	 *  Added for Assignment Module 12.
	*/
	@Override
	public void accept(NodeVisitor nodeVisitor) {
		nodeVisitor.visitNode(this);
	}

	@Override
	public Node duplicate(edu.jhu.apl.patterns_class.dom.replacement.Document document) {
		return getElement().duplicate(document);
	}

	/**
	 * If there is no element, use the Builder to create a subtree.
	 */
	private ElementInterface getElement() {
		System.out.println("ElementProxy.getElement()");

		if (this.element == null) {
			AbstractDOMBuilder builder = new ConcreteDOMBuilder(new ConcreteDOMFactory());
			DOMDirector director = new DOMDirector(builder);
			try {
				director.construct(filename, tokenIndex);
			} catch (IOException e) {
				System.out.println("Error reading file while constructing subtree.");
				e.printStackTrace();
			}
			NodeCompositeInterface root = builder.getResult();
			this.element = root.hasChildNodes() ? (ElementInterface) root.getFirstChild() : null;
		}
		return this.element;
	}

	/**
	 * Does nothing; a proxy node should not have proxy children.
	 */
	public Node appendProxyChild(ElementProxy newChild) {
		return newChild;
	}

	@Override
	public ElementProxy clone() {
		return null;
	}

	// Proxied Methods:

	@Override
	public void setParent(Node parent) {
		getElement().setParent(parent);
	}

	@Override
	public edu.jhu.apl.patterns_class.dom.NodeList getChildNodes() {
		return getElement().getChildNodes();
	}

	@Override
	public String getAttribute(String name) {
		return getElement().getAttribute(name);
	}

	@Override
	public String getAttributeNS(String namespaceURI, String localName) {
		return getElement().getAttributeNS(namespaceURI, localName);
	}

	@Override
	public String getNodeName() {
		return getElement().getNodeName();
	}

	@Override
	public String getNodeValue() throws DOMException {
		return getElement().getNodeValue();
	}

	@Override
	public void setNodeValue(String nodeValue) throws DOMException {
		getElement().setNodeValue(nodeValue);
	}

	@Override
	public short getNodeType() {
		return getElement().getNodeType();
	}

	@Override
	public Node getParentNode() {
		return getElement().getParentNode();
	}

	@Override
	public Node getFirstChild() {
		return getElement().getFirstChild();
	}

	@Override
	public Node getLastChild() {
		return getElement().getLastChild();
	}

	@Override
	public Node getPreviousSibling() {
		return getElement().getPreviousSibling();
	}

	@Override
	public Node getNextSibling() {
		return getElement().getNextSibling();
	}

	@Override
	public edu.jhu.apl.patterns_class.dom.replacement.Document getOwnerDocument() {
		return getElement().getOwnerDocument();
	}

	@Override
	public Node insertBefore(Node newChild, Node refChild) throws DOMException {
		return getElement().insertBefore(newChild, refChild);
	}

	@Override
	public Node replaceChild(Node newChild, Node oldChild) throws DOMException {
		return getElement().replaceChild(newChild, oldChild);
	}

	@Override
	public Node removeChild(Node oldChild) throws DOMException {
		return getElement().removeChild(oldChild);
	}

	@Override
	public Node appendChild(Node newChild) throws DOMException {
		return getElement().appendChild(newChild);
	}

	@Override
	public boolean hasChildNodes() {
		return getElement().hasChildNodes();
	}

	@Override
	public String getLocalName() {
		return getElement().getLocalName();
	}

	@Override
	public void normalize() {
		getElement().normalize();
	}

	@Override
	public boolean isSupported(String feature, String version) {
		return getElement().isSupported(feature, version);
	}

	@Override
	public String getNamespaceURI() {
		return getElement().getNamespaceURI();
	}

	@Override
	public String getPrefix() {
		return getElement().getPrefix();
	}

	@Override
	public void setPrefix(String prefix) throws DOMException {
		getElement().setPrefix(prefix);
	}

	@Override
	public Node cloneNode(boolean deep) {
		return getElement().cloneNode(deep);
	}

	@Override
	public Object getUserData(String key) {
		return getElement().getUserData(key);
	}

	@Override
	public Object setUserData(String key, Object data, UserDataHandler handler) {
		return getElement().setUserData(key, data, handler);
	}

	@Override
	public Object getFeature(String feature, String version) {
		return getElement().getFeature(feature, version);
	}

	@Override
	public boolean isEqualNode(Node arg) {
		return getElement().isEqualNode(arg);
	}

	@Override
	public String lookupNamespaceURI(String prefix) {
		return getElement().lookupNamespaceURI(prefix);
	}

	@Override
	public boolean isDefaultNamespace(String namespaceURI) {
		return getElement().isDefaultNamespace(namespaceURI);
	}

	@Override
	public String lookupPrefix(String namespaceURI) {
		return getElement().lookupPrefix(namespaceURI);
	}

	@Override
	public boolean isSameNode(Node other) {
		return getElement().isSameNode(other);
	}

	@Override
	public void setTextContent(String textContent) {
		getElement().setTextContent(textContent);
	}

	@Override
	public String getTextContent() {
		return getElement().getTextContent();
	}

	@Override
	public short compareDocumentPosition(Node other) {
		return getElement().compareDocumentPosition(other);
	}

	@Override
	public String getBaseURI() {
		return getElement().getBaseURI();
	}

	@Override
	public NodeIterator createIterator() throws DOMException {
		return getElement().createIterator();
	}

	@Override
	public Attr getAttributeNode(String name) {
		return getElement().getAttributeNode(name);
	}

	@Override
	public NodeList getElementsByTagName(String tagName) {
		return getElement().getElementsByTagName(tagName);
	}

	@Override
	public String getTagName() {
		return getElement().getTagName();
	}

	@Override
	public boolean hasAttribute(String name) {
		return getElement().hasAttribute(name);
	}

	@Override
	public void removeAttribute(String name) {
		getElement().removeAttribute(name);
	}

	@Override
	public Attr removeAttributeNode(Attr oldAttr) {
		return getElement().removeAttributeNode(oldAttr);
	}

	@Override
	public void setAttribute(String name, String value) {
		getElement().setAttribute(name, value);
	}

	@Override
	public Attr setAttributeNode(Attr newAttr) {
		return getElement().setAttributeNode(newAttr);
	}

	@Override
	public Attr getAttributeNodeNS(String namespaceURI, String localName) {
		return getElement().getAttributeNodeNS(namespaceURI, localName);
	}

	@Override
	public NodeList getElementsByTagNameNS(String tagName) {
		return getElement().getElementsByTagNameNS(tagName);
	}

	@Override
	public boolean hasAttributeNS(String namespaceURI, String localName) {
		return getElement().hasAttributeNS(namespaceURI, localName);
	}

	@Override
	public void removeAttributeNS(String namespaceURI, String localName) {
		getElement().removeAttributeNS(namespaceURI, localName);
	}

	@Override
	public Attr setAttributeNodeNS(Attr newAttr) {
		return getElement().setAttributeNodeNS(newAttr);
	}

	@Override
	public void setAttributeNS(String namespaceURI, String localName, String value) {
		getElement().setAttributeNS(namespaceURI, localName, value);
	}

	@Override
	public Attr setAttributeNS(Attr newAttr) {
		return getElement().setAttributeNS(newAttr);
	}

	@Override
	public NodeList getElementsByTagNameNS(String namespaceURI, String localName) {
		return getElement().getElementsByTagNameNS(namespaceURI, localName);
	}

	@Override
	public void setIdAttributeNode(Attr idAttr, boolean isId) {
		getElement().setIdAttributeNode(idAttr, isId);
	}

	@Override
	public void setIdAttributeNS(String namespaceURI, String localName, boolean isId) {
		getElement().setIdAttributeNS(namespaceURI, localName, isId);
	}

	@Override
	public void setIdAttribute(String name, boolean isId) {
		getElement().setIdAttribute(name, isId);
	}

	@Override
	public TypeInfo getSchemaTypeInfo() {
		return getElement().getSchemaTypeInfo();
	}

	@Override
	public NamedNodeMap getAttributes() {
		return getElement().getAttributes();
	}

	@Override
	public boolean hasAttributes() {
		return getElement().hasAttributes();
	}

	@Override
	public void sendEvent(String eventType) {
		getElement().sendEvent(eventType);
	}

}