package edu.jhu.apl.patterns_class.dom;

import java.util.LinkedList;

import org.w3c.dom.DOMException;

import edu.jhu.apl.patterns_class.support.NodeVisitor;

/** Assignment 2: Made `Node` abstract to represent Component in the Composite pattern.
 * Assignment Module 12: Added `accept` method for Visitor pattern.
*/
public abstract class Node implements edu.jhu.apl.patterns_class.dom.replacement.Node {
	private String name = null;
	private String value = null;
	private short nodeType = -1;
	protected edu.jhu.apl.patterns_class.dom.replacement.Node parent = null;
	protected Document document = null;

	Node(String name, short type) {
		this.name = name;
		nodeType = type;
	}

	public String toString() {
		return "Node, class: " + this.getClass() + " (name: " + this.getNodeName() + ", value: " + this.getNodeValue() + ")";
	}

	/**
	 *  Added for Assignment Module 12.
	*/
	@Override
	public void accept(NodeVisitor nodeVisitor) {
		nodeVisitor.visitNode(this);
	}

	public void setParent(edu.jhu.apl.patterns_class.dom.replacement.Node parent) {
		this.parent = parent;
	}

	public Node appendProxyChild(ElementProxy newChild) {
		return null;
	}

	//
	// Implemented Interface Members
	//
	public String getNodeName() {
		return name;
	}

	public String getNodeValue() throws DOMException {
		return value;
	}

	public void setNodeValue(String nodeValue) throws DOMException {
		value = nodeValue;
	}

	public short getNodeType() {
		return nodeType;
	}

	public edu.jhu.apl.patterns_class.dom.replacement.Node getParentNode() {
		return parent;
	}

	public abstract edu.jhu.apl.patterns_class.dom.replacement.NodeList getChildNodes();

	public abstract edu.jhu.apl.patterns_class.dom.replacement.Node getFirstChild();

	public abstract edu.jhu.apl.patterns_class.dom.replacement.Node getLastChild();

	public edu.jhu.apl.patterns_class.dom.replacement.Node getPreviousSibling() {
		return (edu.jhu.apl.patterns_class.dom.replacement.Node) getSibling(-1);
	}

	public edu.jhu.apl.patterns_class.dom.replacement.Node getNextSibling() {
		return (edu.jhu.apl.patterns_class.dom.replacement.Node) getSibling(1);
	}

	public edu.jhu.apl.patterns_class.dom.replacement.Document getOwnerDocument() {
		return document;
	}

	public abstract edu.jhu.apl.patterns_class.dom.replacement.Node insertBefore(
			edu.jhu.apl.patterns_class.dom.replacement.Node newChild,
			edu.jhu.apl.patterns_class.dom.replacement.Node refChild) throws DOMException;

	public abstract edu.jhu.apl.patterns_class.dom.replacement.Node replaceChild(
			edu.jhu.apl.patterns_class.dom.replacement.Node newChild,
			edu.jhu.apl.patterns_class.dom.replacement.Node oldChild) throws DOMException;

	public abstract edu.jhu.apl.patterns_class.dom.replacement.Node removeChild(
			edu.jhu.apl.patterns_class.dom.replacement.Node oldChild) throws DOMException;

	public abstract edu.jhu.apl.patterns_class.dom.replacement.Node appendChild(
			edu.jhu.apl.patterns_class.dom.replacement.Node newChild) throws DOMException;

	public abstract boolean hasChildNodes();

	public String getLocalName() {
		return name;
	}

	//
	// Unimplemented Interface Members
	//
	public void normalize() {
	}

	public boolean isSupported(String feature, String version) {
		return false;
	}

	public String getNamespaceURI() {
		return null;
	}

	public String getPrefix() {
		return null;
	}

	public void setPrefix(String prefix) throws DOMException {
	}

	public edu.jhu.apl.patterns_class.dom.replacement.Node cloneNode(boolean deep) {
		return null;
	}

	public boolean hasAttributes() {
		return false;
	}

	public edu.jhu.apl.patterns_class.dom.replacement.NamedNodeMap getAttributes() {
		return null;
	}

	public Object getUserData(String key) {
		return null;
	}

	public Object setUserData(String key, Object data, org.w3c.dom.UserDataHandler handler) {
		return null;
	}

	public Object getFeature(String feature, String version) {
		return null;
	}

	public boolean isEqualNode(edu.jhu.apl.patterns_class.dom.replacement.Node arg) {
		return false;
	}

	public String lookupNamespaceURI(String prefix) {
		return null;
	}

	public boolean isDefaultNamespace(String namespaceURI) {
		return false;
	}

	public String lookupPrefix(String namespaceURI) {
		return null;
	}

	public boolean isSameNode(edu.jhu.apl.patterns_class.dom.replacement.Node other) {
		return false;
	}

	public void setTextContent(String textContent) {
	}

	public String getTextContent() {
		return null;
	}

	public short compareDocumentPosition(edu.jhu.apl.patterns_class.dom.replacement.Node other) {
		return (short) 0;
	}

	public String getBaseURI() {
		return null;
	}

	public abstract NodeIterator createIterator() throws DOMException;

	//
	// Class Members
	//
	private edu.jhu.apl.patterns_class.dom.replacement.Node getSibling(int direction) {
		if (parent == null)
			return null;

		LinkedList<edu.jhu.apl.patterns_class.dom.replacement.Node> siblings = (LinkedList<edu.jhu.apl.patterns_class.dom.replacement.Node>) parent.getChildNodes();

		try {
			return siblings.get(siblings.indexOf(this) + direction);
		} catch (java.lang.IndexOutOfBoundsException e) {
			return null;
		}
	}
}
