package edu.jhu.apl.patterns_class;

import edu.jhu.apl.patterns_class.dom.replacement.Node;
import edu.jhu.apl.patterns_class.dom.replacement.Document;
import edu.jhu.apl.patterns_class.dom.replacement.Element;
import edu.jhu.apl.patterns_class.dom.NodeIterator;
import edu.jhu.apl.patterns_class.dom.replacement.Text;
import edu.jhu.apl.patterns_class.support.NodeVisitor;
import edu.jhu.apl.patterns_class.support.SerializationNodeVisitor;
import edu.jhu.apl.patterns_class.dom.replacement.Attr;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

/**
 * Takes an input DOM structure and writes it to an XML file. (The inverese of
 * XMLTokenizer.)
 */
public class XMLSerializer {
	File file = null;
	BufferedWriter writer = null;
	public int indentationLevel = 0;
	private String filename = null;

	public XMLSerializer(String filename) throws FileNotFoundException {
		this.filename = filename;
		this.emerge();
	}

	/**
	 * Assignment 5: Implementing the idiom Helper Method for Initializations.
	 */
	private void emerge() throws FileNotFoundException {
		this.file = new File(this.filename);
		this.writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.file)));
	}

	public void close() throws IOException {
		writer.close();
	}

	public String getPrettyIndentation() {
		String str = "";
		for (int i = 0; i < indentationLevel; i++)
			str += "\t";
		return str;
	}

	public void serializePretty(Node node) throws IOException {
		this.serialize(node, true);
	}

	public void serializeMinimal(Node node) throws IOException {
		this.serialize(node, false);
	}

	public void serialize(Node node, boolean pretty) throws IOException {

		// Assignment 2: Using `NodeIterator` rather than creating our own iterator.
		NodeIterator iterator = node.createIterator();

		// Assignment Module 11: Begin serialization visiting.
		NodeVisitor serializationVisitor = new SerializationNodeVisitor(iterator, writer, pretty, this);

	}

	/**
	 * This method tests correct functioning of the Serializer .
	 *
	 * @param args Two args are required; each represents a file that will be
	 *             written. The first is written with Pretty printing; the second
	 *             with Minimal printing.
	 */
	public static void main(String args[]) {
		if (args.length < 2) {
			System.out.println("No output filenames provided.");
			System.exit(0);
		}

		//
		// Create tree of this document:
		// <? xml version="1.0" encoding="UTF-8"?>
		// <document>
		// <element attribute="attribute value"/>
		// <element/>
		// <element attribute="attribute value" attribute2="attribute2 value">
		// Element Value
		// </element>
		// <element>
		// </element>
		// </document>
		//
		Document document = new edu.jhu.apl.patterns_class.dom.Document();
		Element root = document.createElement("document");
		document.appendChild(root);

		Element child = document.createElement("element");
		Attr attr = document.createAttribute("attribute");
		attr.setValue("attribute value");
		child.setAttributeNode(attr);
		root.appendChild(child);

		child = document.createElement("element");
		root.appendChild(child);

		child = document.createElement("element");
		child.setAttribute("attribute", "attribute value");
		child.setAttribute("attribute2", "attribute2 value");
		Text text = document.createTextNode("Element Value");
		child.appendChild(text);
		root.appendChild(child);

		child = document.createElement("element");
		root.appendChild(child);

		//
		// Serialize
		//
		try {
			XMLSerializer xmlSerializer = new XMLSerializer(args[0]);
			xmlSerializer.serializePretty(document);
			xmlSerializer.close();
			xmlSerializer = new XMLSerializer(args[1]);
			xmlSerializer.serializeMinimal(document);
			xmlSerializer.close();
		} catch (IOException e) {
			System.out.println("Error writing file.");
			e.printStackTrace();
		}
	}
}
