package edu.jhu.apl.patterns_class.support.commands;

import java.io.IOException;
import java.util.ArrayList;

import edu.jhu.apl.patterns_class.XMLTokenizer;
import edu.jhu.apl.patterns_class.XMLTokenizer.XMLToken;

/**
 * Added for Assignment Module 11.
 * Acts as `Command` in the `Command` pattern by providing the interface for the command. Each instance is a `ConcreteCommand`.
 */
public class ParseFileCommand {
    private XMLTokenizer receiver = null;

    public ParseFileCommand() { }

    public void setReceiver(XMLTokenizer receiver) {
        this.receiver = receiver;
    }

    public ArrayList<XMLTokenizer.XMLToken> execute() throws IOException {
        ArrayList<XMLTokenizer.XMLToken> tokens = new ArrayList<>();
        XMLTokenizer.XMLToken token = null;
        do {
            token = receiver.getNextToken();
            tokens.add(token);
        } while (token.getTokenType() != XMLToken.NULL);

        return tokens;
    }
}