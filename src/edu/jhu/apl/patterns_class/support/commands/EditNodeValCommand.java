package edu.jhu.apl.patterns_class.support.commands;


import edu.jhu.apl.patterns_class.dom.NodeCompositeInterface;

/**
 * Added for Assignment Module 11.
 * Acts as `Command` in the `Command` pattern by providing the interface for the command. Each instance is a `ConcreteCommand`.
 */
public class EditNodeValCommand {
    private NodeCompositeInterface receiver = null;

    public EditNodeValCommand() { }

    public void setReceiver(NodeCompositeInterface receiver) {
        this.receiver = receiver;
    }

    public NodeCompositeInterface execute(String nodeValue) {
        receiver.setNodeValue(nodeValue);
        return receiver;
    }
}