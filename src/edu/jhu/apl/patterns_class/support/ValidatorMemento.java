package edu.jhu.apl.patterns_class.support;

import java.util.Vector;

/**
 * Added for Assignment Module 9.
 * Implements Memento in the Memento pattern.
 * Stores and allows retrieval of the state of the XMLValidator, which is limited to the `schema` property for this implementation.
 */
public class ValidatorMemento {

    private Vector<ValidChildren> schema = null;

    /**
     * Returns the schema.
     */
    public Vector<ValidChildren> getState() {
        return this.schema;
    }

    /**
     * Sets the schema.
     * Note: A robust implementation of this should perform a deep clone of `schema` to avoid the situation where schema elements could be modified by external objects. This is omitted in the current implementation, as deep cloning is complex to implement and seemingly outside the scope of this assignment. Real implementations would use external deep clone libraries, ideally incorporated via a package manager.
     */
    public void setState(Vector<ValidChildren> schema) {
        this.schema = schema;
    }

}