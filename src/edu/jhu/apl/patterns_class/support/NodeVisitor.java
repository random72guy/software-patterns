package edu.jhu.apl.patterns_class.support;

import edu.jhu.apl.patterns_class.dom.replacement.Node;

/**
 * Created for Assignment Module 12.
 * Implements Visitor in the Visitor pattern.
 */
public abstract class NodeVisitor {
    public abstract void visitNode(Node n);
}