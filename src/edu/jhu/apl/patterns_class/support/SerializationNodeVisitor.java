package edu.jhu.apl.patterns_class.support;

import java.io.BufferedWriter;
import java.io.IOException;

import edu.jhu.apl.patterns_class.XMLSerializer;
import edu.jhu.apl.patterns_class.dom.NodeIterator;
import edu.jhu.apl.patterns_class.dom.replacement.Node;

/**
 * Created for Assignment Module 12.
 * Implements ConcreteVisitor in the Visitor pattern.
 */
public class SerializationNodeVisitor extends NodeVisitor {

    NodeIterator iterator;
    BufferedWriter writer;
    boolean pretty;
    XMLSerializer xmlSerializer;

    /**
     * Creates the Visitor and begins visiting.
     */
    public SerializationNodeVisitor(NodeIterator iterator, BufferedWriter writer, boolean pretty, XMLSerializer xmlSerializer) {
        this.iterator = iterator;
        this.writer = writer;
        this.pretty = pretty;
        this.xmlSerializer = xmlSerializer;

		if (this.iterator.hasNext())
			this.iterator.next().accept(this);
    }

    @Override
    public void visitNode(Node n) {

        // Assignment 1: Implemented strategy to read and write DOM elements.
        SerializationStrategy strategy = SerializationStrategy.getStrategy(n);

        // Assignment 3: Implemented a Template Method for serialization algorithm.
        String str = strategy.getTemplate().serialize(n, pretty, !iterator.hasNext(), xmlSerializer);

        try {
            writer.write(str);
        } catch (IOException e) {
            System.out.println("Error node writing serialization: " + e);
        }


        if (iterator.hasNext())
			iterator.next().accept(this);
    }
}