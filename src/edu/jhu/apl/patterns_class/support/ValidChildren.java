package edu.jhu.apl.patterns_class.support;

import java.util.Vector;

/**
 * Assignment 1: Converting this to be a Decorator so it can handle validation.
 * Assignment Module 9:
 */
public class ValidChildren {
    private String thisElement = null; // A value of null represents Document.
    private Vector<String> validChildren = new Vector<String>();
    private Vector<Boolean> childIsAttribute = new Vector<Boolean>();
    private boolean _canHaveText = false;

    public ValidChildren(String thisElement) {
        this.thisElement = thisElement;
    }

    public String getThisElement() {
        return thisElement;
    }

    public boolean canHaveText() {
        return _canHaveText;
    }

    public void setCanHaveText(boolean _canHaveText) {
        this._canHaveText = _canHaveText;
    }

    public void addValidChild(String child, boolean isAttribute) {
        if (childIsValid(child, isAttribute))
            return;

        validChildren.add(child);
        childIsAttribute.add(new Boolean(isAttribute));
    }

    //
    /**
     * Checks whether the child is in the list of valid children. If `isAttribute`
     * is true, checks the list of attributes. Else checks the list of
     * validChildren. TODO: Pattern Opportunity: Decorator. This should probably be
     * more tied to // the nodes, and run recursively, rather than doing a lookup.
     */
    public boolean childIsValid(String child, boolean isAttribute) {

        // Pattern Opportunity: Iterator
        // Returns True if each Attr is present.
        for (int i = 0; i < validChildren.size(); i++)
            if (childIsAttribute.elementAt(i).booleanValue() == isAttribute
                    && validChildren.elementAt(i).compareTo(child) == 0)
                return true;

        return false;

    }
}