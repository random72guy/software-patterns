package edu.jhu.apl.patterns_class.support.state;

import java.io.IOException;

import org.w3c.dom.DOMException;

import edu.jhu.apl.patterns_class.XMLTokenizer;
import edu.jhu.apl.patterns_class.dom.ElementProxy;

/**
 * Created for Assignment Module 12 State pattern. Implements State in the
 * pattern.
 */
public class InsideAttrBuildState extends BuildState {

    public void handle(BuildContext buildContext) throws IOException {
        String attrName = buildContext.token.getToken();
        buildContext.token = buildContext.tokenizer.getNextToken();

				if (buildContext.token.getTokenType() != XMLTokenizer.XMLToken.ATTRIBUTE_VALUE)
					throw new DOMException((short) 12, "Attr requires a value.");

				if (!(buildContext.parentNodeStack.peek() instanceof ElementProxy)) {
					String attrVal = buildContext.token.getToken();
					buildContext.builder.buildAttr(buildContext.parentNodeStack.peek(), attrName, attrVal);
				}

    }

}
