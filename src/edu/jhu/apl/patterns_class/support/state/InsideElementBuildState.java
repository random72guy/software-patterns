package edu.jhu.apl.patterns_class.support.state;

import java.io.IOException;

import edu.jhu.apl.patterns_class.XMLTokenizer.XMLToken;
import edu.jhu.apl.patterns_class.dom.ElementInterface;
import edu.jhu.apl.patterns_class.dom.NodeCompositeInterface;

/**
 * Created for Assignment Module 12 State pattern. Implements State in the
 * pattern.
 */
public class InsideElementBuildState extends BuildState {

    public void handle(BuildContext buildContext) throws IOException {
        XMLToken token = buildContext.tokenizer.getNextToken();
        String elmName = token.getToken();

        // Only ever scan one element deep.
            // Create virtual proxies for every sub-Element.
            NodeCompositeInterface top = buildContext.parentNodeStack.peek();

            System.out.println("Top: " + top + ", Building element: " + elmName);

            ElementInterface newElm = buildContext.builder.buildElement(top, elmName);

            buildContext.parentNodeStack.push(newElm);
            // buildContext.parentNode = buildContext.elementDepth == 0
            //         ? buildContext.builder.buildElement(buildContext.parentNode, elmName)
            //         : buildContext.builder.buildProxyElement(buildContext.parentNode, elmName,
            //                 buildContext.tokenizer.getLineNumber(), buildContext.filename);

    }

}
