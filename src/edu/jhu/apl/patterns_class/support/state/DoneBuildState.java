package edu.jhu.apl.patterns_class.support.state;

/**
 * Created for Assignment Module 12 State pattern. Implements ConcreteState in
 * the pattern. Does nothing; simply signifies state.
 */
public class DoneBuildState extends BuildState {

    public void handle(BuildContext buildContext) {
    };
}
