package edu.jhu.apl.patterns_class.support.state;

import java.io.IOException;

import org.w3c.dom.DOMException;

import edu.jhu.apl.patterns_class.dom.ElementInterface;
import edu.jhu.apl.patterns_class.dom.ElementProxy;

/**
 * Created for Assignment Module 12 State pattern. Implements State in the
 * pattern.
 */
public class InsideTextBuildState extends BuildState {

    public void handle(BuildContext buildContext) throws IOException {
        if (buildContext.parentNodeStack.peek() == null || !(buildContext.parentNodeStack.peek() instanceof ElementInterface))
            throw new DOMException((short) 3, "Text must be added to an Element");

        if (!(buildContext.parentNodeStack.peek() instanceof ElementProxy)) {
            buildContext.builder.buildText((ElementInterface) buildContext.parentNodeStack.peek(), buildContext.token.getToken());
        }

    }

}
