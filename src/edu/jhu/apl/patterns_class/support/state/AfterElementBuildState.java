package edu.jhu.apl.patterns_class.support.state;

import java.io.IOException;

/**
 * Created for Assignment Module 12 State pattern. Implements State in the
 * pattern.
 */
public class AfterElementBuildState extends BuildState {

    public void handle(BuildContext buildContext) throws IOException {
    }

}
