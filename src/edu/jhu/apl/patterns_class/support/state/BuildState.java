package edu.jhu.apl.patterns_class.support.state;

import java.io.IOException;

/**
 * Created for Assignment Module 12 State pattern.
 * Implements State in the pattern.
 */
public abstract class BuildState {
    public abstract void handle(BuildContext buildContext) throws IOException;
}
