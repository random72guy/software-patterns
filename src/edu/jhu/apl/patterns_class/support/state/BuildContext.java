package edu.jhu.apl.patterns_class.support.state;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import edu.jhu.apl.patterns_class.XMLTokenizer;
import edu.jhu.apl.patterns_class.dom.AbstractDOMBuilder;
import edu.jhu.apl.patterns_class.dom.NodeCompositeInterface;

/**
 * Implemented for Assignment Module 12 State pattern.
 * Maintains a reference to the active State, and provides the ability to change the state.
 */
public class BuildContext {

    public BuildState state;
    XMLTokenizer tokenizer;
    private Map<Integer, BuildState> stateMap = new HashMap<>();
    public Stack<NodeCompositeInterface> parentNodeStack = new Stack<>();
    protected XMLTokenizer.XMLToken token;
    protected AbstractDOMBuilder builder;
    protected String filename;

    public BuildContext(XMLTokenizer tokenizer, AbstractDOMBuilder builder, String filename) throws IOException {
        this.builder = builder;
        this.filename = filename;
        this.tokenizer = tokenizer;
        this.token = this.tokenizer.getNextToken();
        this.parentNodeStack.push(builder.buildDocument());

        // Build state map:
        stateMap.put(XMLTokenizer.XMLToken.NULL, new DoneBuildState());
        stateMap.put(XMLTokenizer.XMLToken.PROLOG_START, new InsideDocumentBuildState());
        stateMap.put(XMLTokenizer.XMLToken.TAG_START, new InsideElementBuildState());
        stateMap.put(XMLTokenizer.XMLToken.TAG_END, new AfterElementBuildState());
        stateMap.put(XMLTokenizer.XMLToken.TAG_CLOSE_START, new AfterElementCloseBuildState());
        stateMap.put(XMLTokenizer.XMLToken.ATTRIBUTE, new InsideAttrBuildState());
        stateMap.put(XMLTokenizer.XMLToken.VALUE, new InsideTextBuildState());
    }

    public void build(int startingLineNumber) throws IOException {
        // Skip to designated starting line number.
        if (tokenizer.getLineNumber() < startingLineNumber) {
            token = tokenizer.getNextToken();
            System.out.println("Skipping line " + tokenizer.getLineNumber() + ". Target: "+ startingLineNumber);
        } else {
            System.out.println("Tokenizing line " + tokenizer.getLineNumber() + ": " + token.getToken());
        }
        changeState(token.getTokenType());
        if (state != null) state.handle(this);
        token = tokenizer.getNextToken();
    }

    public void changeState(Integer tokenId) {
        this.state = this.stateMap.get(tokenId);
    }
}
