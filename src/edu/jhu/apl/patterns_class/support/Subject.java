package edu.jhu.apl.patterns_class.support;

import java.util.ArrayList;
import java.util.List;

/**
 * Created for Assignment 7. Implements Subject in Observer pattern.
 * Generics are used to ensure that the Observer knows the type of subject so that it may access information, methods, etc.
 */
public abstract class Subject<T> {

    private List<Observer<T>> observers = new ArrayList<>();

    public void attach(Observer<T> o) {
        observers.add(o);
    }

    public void detatch(Observer<T> o) {
        observers.remove(o);
    }

    public void notifyObservers(String eventType) {
        observers.forEach(observer -> {
            observer.update(eventType, (T) this);
        });
    }

}