package edu.jhu.apl.patterns_class.support;

import edu.jhu.apl.patterns_class.dom.replacement.Node;
import edu.jhu.apl.patterns_class.dom.replacement.Element;
import edu.jhu.apl.patterns_class.XMLSerializer;
import edu.jhu.apl.patterns_class.dom.ElementInterface;
import edu.jhu.apl.patterns_class.dom.NodeComposite;
import edu.jhu.apl.patterns_class.dom.NodeList;
import edu.jhu.apl.patterns_class.dom.replacement.Text;
import edu.jhu.apl.patterns_class.dom.replacement.Attr;
import java.util.Iterator;


/**
 * Created for Assignment 1. Reads a node and writes the appropriate output.
 * Implements the Strategy pattern. Note that this is an Abstract Class rather
 * than an Interface in case certain logic is common to all strategies and ought
 * to be abstracted. At this time, it seems less verbose to implement this as a
 * Flyweight than as a more heavyweight object.
 */
public abstract class SerializationStrategy<T extends Node> {

	public abstract AbstractSerializationTemplate getTemplate();

	/**
	 * Factory method to get the correct strategy.
	 */
	public static SerializationStrategy getStrategy(Node node) {
		if (node instanceof edu.jhu.apl.patterns_class.dom.Document)
			return new DocumentSerializationStrategy();
		else if (node instanceof Element)
			return new ElementSerializationStrategy();
		else if (node instanceof Attr)
			return new AttrSerializationStrategy();
		else if (node instanceof Text)
			return new TextSerializationStrategy();

		return null;
	}

}

class DocumentSerializationStrategy extends SerializationStrategy<edu.jhu.apl.patterns_class.dom.Document> {
	@Override
	public AbstractSerializationTemplate getTemplate() {
		return new DocumentSerializationTemplate();
	}
}

class ElementSerializationStrategy extends SerializationStrategy<Element> {
	@Override
	public AbstractSerializationTemplate getTemplate() {
		return new ElementSerializationTemplate();
	}

}

class AttrSerializationStrategy extends SerializationStrategy<Attr> {
	@Override
	public AbstractSerializationTemplate getTemplate() {
		return new AttrSerializationTemplate();
	}

}

class TextSerializationStrategy extends SerializationStrategy<Text> {
	@Override
	public AbstractSerializationTemplate getTemplate() {
		return new TextSerializationTemplate();
	}
}

/**
 * Created for Assignment 3. A serialization template `AbstractClass`. The
 * invariant is mainly the need to write a string per node. Variants are the
 * content to write per node, and the content to write after each node. While
 * pretty-printing must remain an optional parameter, each `PrimitiveOperation`
 * may need to handle this differently, and should not be able to choose whether
 * they pretty print or not, but only the way in which they pretty-print when
 * required.
 */
abstract class AbstractSerializationTemplate {

	// These properties may need to be used by primitive operations.
	protected Node node = null;
	protected boolean pretty = false;
	protected XMLSerializer serializer = null;

	// This is our main template method.
	public final String serialize(Node node, boolean pretty, boolean isLastNode, XMLSerializer serializer) {
		this.node = node;
		this.pretty = pretty;
		this.serializer = serializer;

		String str = "";

		str += this.getContentStr();

		if (pretty)
			str += this.getNewlineStr();

		this.afterNewline();

		// TODO: Fix bug where document may not have child nodes. (This case should never occur.)
		if (isLastNode)
			str += "</" + node.getOwnerDocument().getFirstChild().getNodeName() + ">";

		return str;
	}

	protected abstract String getContentStr();

	// Default implementation provided because most Nodes should be preceded by a
	// newline, with the exception of Attr.
	protected String getNewlineStr() {
		return "\n";
	};

	protected void afterNewline() {
	}

}

class DocumentSerializationTemplate extends AbstractSerializationTemplate {
	@Override
	protected String getContentStr() {
		return "";
	}
}

class ElementSerializationTemplate extends AbstractSerializationTemplate {
	@Override
	protected String getContentStr() {

		String str = "";
		Element element = (ElementInterface) node;

		if (pretty) {
			Node parent = element.getParentNode();
			// System.out.println("Handling "+ element.)
			// TODO: Fix bug where document may not have child nodes. (This case should never occur.)
			if (parent != null && parent.getFirstChild() == element && parent instanceof NodeComposite
					&& element.getOwnerDocument().getFirstChild() != element) {
				serializer.indentationLevel++;
			}
			str += serializer.getPrettyIndentation();
		}

		str += "<" + element.getTagName();

		if (element.hasAttributes()) {
			Iterator<Node> attrIterator = ((NodeList) element.getAttributes()).listIterator();
			AttrSerializationStrategy strategy = new AttrSerializationStrategy();
			while (attrIterator.hasNext()) {
				Attr attr = (Attr) attrIterator.next();
				str += strategy.getStrategy(attr).getTemplate().serialize(attr, pretty, false, serializer);
			}
		}

		// Close the element.
		boolean hasChildElements = element.getChildNodes().getLength() > 0;
		str += hasChildElements ? ">" : "/>";

		return str;
	}
}

class AttrSerializationTemplate extends AbstractSerializationTemplate {
	@Override
	protected String getContentStr() {
		Attr attr = (Attr) node;
		return " " + attr.getName() + "=\"" + attr.getValue() + "\"";
	}

	@Override
	protected String getNewlineStr() {
		return "";
	}
}

class TextSerializationTemplate extends AbstractSerializationTemplate {
	@Override
	protected String getContentStr() {

		String str = "";

		if (pretty) {
			serializer.indentationLevel++;
			str += serializer.getPrettyIndentation();
		}

		Text text = (Text) node;
		str += text.getData();

		return str;
	}

	@Override
	protected void afterNewline() {
		if (pretty)
			serializer.indentationLevel--;
	}
}
