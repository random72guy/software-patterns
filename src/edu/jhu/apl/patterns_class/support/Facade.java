package edu.jhu.apl.patterns_class.support;

import java.io.IOException;

import edu.jhu.apl.patterns_class.XMLSerializer;
import edu.jhu.apl.patterns_class.XMLValidator;
import edu.jhu.apl.patterns_class.dom.ConcreteDOMBuilder;
import edu.jhu.apl.patterns_class.dom.ConcreteDOMFactory;
import edu.jhu.apl.patterns_class.dom.DOMDirector;
import edu.jhu.apl.patterns_class.dom.NodeCompositeInterface;
import edu.jhu.apl.patterns_class.dom.replacement.Document;
import edu.jhu.apl.patterns_class.support.validation.DocumentValidationDecorator;

/**
 * Created for Assignment Module 13. Implements the Facade pattern. Provides
 * access to the application's main functionality.
 */
public class Facade {

    /**
     * Serializes a document to the file provided.
     */
    public void xmlSerialize(Document document, String outputFilename, Boolean prettyPrint) {
        try {
            XMLSerializer xmlSerializer = new XMLSerializer(outputFilename);
            if (prettyPrint)
                xmlSerializer.serializePretty(document);
            else
                xmlSerializer.serializeMinimal(document);
            xmlSerializer.close();
        } catch (IOException e) {
            System.out.println("Error writing file.");
            e.printStackTrace();
        }
    }

    /**
     * Tokenizes a file file and returns the generated DOM tree.
     */
    public NodeCompositeInterface xmlTokenize(String filename) throws IOException {

        ConcreteDOMBuilder builder = new ConcreteDOMBuilder(new ConcreteDOMFactory());

        DOMDirector director = new DOMDirector(builder);

        director.construct(filename, 0);

        NodeCompositeInterface domTree = builder.getResult();

        return domTree;

    }

    /**
     * Returns an XML Document that will validate any DOM Elements, Attrs, or Text added to it.
     */
    public Document getValidationDocument(ValidChildren schemaRoot) {
		XMLValidator xmlValidator = new XMLValidator();

		Document vXmlDocument = new DocumentValidationDecorator(xmlValidator);

        return vXmlDocument;
    }


}