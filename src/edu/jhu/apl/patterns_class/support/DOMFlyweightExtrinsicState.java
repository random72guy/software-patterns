package edu.jhu.apl.patterns_class.support;

import edu.jhu.apl.patterns_class.dom.replacement.Document;
import edu.jhu.apl.patterns_class.dom.replacement.Node;
import edu.jhu.apl.patterns_class.dom.replacement.NodeList;

/**
 * Created for Assignment Module 13. Defines the extrinsic state of a DOM Flyweight, required for certain operations.
 */
public abstract class DOMFlyweightExtrinsicState {
    public Document document;
    public Node parent;
    public NodeList children;

    public boolean equals(DOMFlyweightExtrinsicState other) {
        return document.equals(other.document) && parent.equals(other.parent) && children.equals(children);
    }
}
