package edu.jhu.apl.patterns_class.support;

import java.util.ArrayList;
import java.util.List;

import edu.jhu.apl.patterns_class.dom.AbstractDOMBuilder;

/**
 * Created for Assignment 7. Emulates a GUI. Creates multiple GUI components to
 * observe AbstractDOMBuilder construction events. Acts as a Concrete Mediator
 * for MockGUIComponents (observers), allowing them to signal all other
 * components whenever they observe a change. Abstract Mediator was deemed
 * unnecessary for this implementation.
 */
public class MockGUI {

    private List<MockGUIComponent> components = new ArrayList<>();

    public MockGUI() { }

    public void attachSubject(Subject<AbstractDOMBuilder> subject) {
        this.components.add(new MockGUIComponent(this, "Element", "buildElement"));
        this.components.add(new MockGUIComponent(this, "ProxyElement", "buildProxyElement"));
        this.components.add(new MockGUIComponent(this, "Text", "buildText"));
        this.components.add(new MockGUIComponent(this, "Attr", "buildAttr"));
        this.components.add(new MockGUIComponent(this, "Document", "buildDocument"));

        this.components.forEach(comp -> subject.attach(comp));
    }

    /**
     * Used by MockGUIComponents to broadcast a message to all other components.
     * This is a contrived example; actual implementations would likely pass more
     * than a string.
     */
    public void broadcastToComponents(String msg) {
        this.components.forEach(component -> component.onOtherComponentActivation(msg));
    }

}

/**
 * Created for Assignment 7. Represents a mock GUI component that observes
 * changes signaled by the `AbstractDOMBuilder`. An implementation decision was
 * made to have all observers receive all event signals and filter for the ones
 * they are interested in; this allows the `Observer` interface to remain simple
 * while allowing flexible capability. This implementation is an admittedly
 * brittle proof-of-concept, as strings are used to indicate event types and the
 * separate strings could become desynchronized without causing a compilation
 * error.
 */
class MockGUIComponent implements Observer<AbstractDOMBuilder> {

    String name;
    String observedEventType;
    MockGUI parentGUI;

    public MockGUIComponent(MockGUI parentGUI, String name, String observedEventType) {
        this.parentGUI = parentGUI;
        this.name = name;
        this.observedEventType = observedEventType;
    }

    @Override
    public void update(String eventType, AbstractDOMBuilder subject) {

        // Ignore event types we aren't listening for.
        if (eventType != this.observedEventType)
            return;

        String msg = this.toString() + " received a " + eventType + " event from the Abstract DOM builder!";

        // Write to std out.
        System.out.println(msg);

        // Signal change to other components via Mediator.
        this.parentGUI.broadcastToComponents(msg);

    }

    /**
     * Called by the Mediator when other components are activated. This
     * proof-of-concept implementation merely indicates that a mediate message was
     * received.
     */
    public void onOtherComponentActivation(String incomingMsg) {
        String ownMsg = this.toString() + " received a message from another component: ";
        System.out.println(ownMsg + incomingMsg);
    }

    public String toString() {
        return "GUI component " + this.name;
    }

}
