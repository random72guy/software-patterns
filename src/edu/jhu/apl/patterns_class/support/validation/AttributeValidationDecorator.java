package edu.jhu.apl.patterns_class.support.validation;

public class AttributeValidationDecorator extends edu.jhu.apl.patterns_class.dom.Attr {

	DocumentValidationDecorator vDoc = null;

	AttributeValidationDecorator(String tagName, DocumentValidationDecorator document) {
		super(tagName, document);
		this.vDoc = document;
	}

}