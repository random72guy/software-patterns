package edu.jhu.apl.patterns_class.support.validation;

import org.w3c.dom.DOMException;

import edu.jhu.apl.patterns_class.XMLValidator;
import edu.jhu.apl.patterns_class.dom.replacement.Element;

/**
 * Adds validation to a Document DOM class.
 */
public class DocumentValidationDecorator extends edu.jhu.apl.patterns_class.dom.Document {

	edu.jhu.apl.patterns_class.dom.Document document = new edu.jhu.apl.patterns_class.dom.Document();
	XMLValidator xmlValidator = null;

	private boolean hasRoot = false;

	public DocumentValidationDecorator(XMLValidator xmlValidator) {
		this.xmlValidator = xmlValidator;
	}

	@Override
	public Element createElement(String tagName) throws DOMException {

		if (this.hasRoot || this.xmlValidator.canRootElement(tagName)) {
			this.hasRoot = true;
			return new ElementValidationDecorator(tagName, this);
		} else {
			System.out.println("Attempted invalid schema operation: Document.createElement " + tagName);
			System.exit(0);
			return null;
		}
	}

}