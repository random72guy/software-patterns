package edu.jhu.apl.patterns_class.support.validation;

import org.w3c.dom.DOMException;

import edu.jhu.apl.patterns_class.dom.ElementInterface;
import edu.jhu.apl.patterns_class.dom.replacement.Element;
import edu.jhu.apl.patterns_class.dom.replacement.Node;
import edu.jhu.apl.patterns_class.dom.replacement.Text;

public class ElementValidationDecorator extends edu.jhu.apl.patterns_class.dom.Element implements ElementInterface {

	private DocumentValidationDecorator vDoc = null;

	ElementValidationDecorator(String tagName, DocumentValidationDecorator document) {
		super(tagName, document);
		this.vDoc = document;
	}

	@Override
	public void setAttribute(String name, String value) {
		if (this.vDoc.xmlValidator.canAddAttribute(this, name)) {
			super.setAttribute(name, value);
		} else {
			System.out.println("Attempted invalid schema operation: setAttribute(" + name + " " + value);
			System.exit(0);
		}
	}

	@Override
	public Node appendChild(Node newChild) throws DOMException {
		boolean valid = true;
		if (newChild instanceof Text && !this.vDoc.xmlValidator.canAddText(this))
			valid = false;
		if (newChild instanceof Element
				&& !this.vDoc.xmlValidator.canAddElement((Element) this, newChild.getNodeName()))
			valid = false;

		if (valid) {
			return super.appendChild(newChild);
		} else {
			System.out.println("Attempted invalid schema operation: Element.appendChild " + newChild.getTextContent());
			System.exit(0);
			return null;
		}
	}

}
