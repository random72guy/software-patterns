package edu.jhu.apl.patterns_class.support;

/**
 * Created for Assignment 7. Implements Observer in Observer pattern.
 * The generic T should correspond to the type of the subject.
 */
public interface Observer<T> {
    public void update(String eventType, T subject);
}