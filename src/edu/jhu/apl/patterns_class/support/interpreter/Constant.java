package edu.jhu.apl.patterns_class.support.interpreter;

/**
 * Created for Assignment Module 13 Interpreter pattern.
 * Implements TerminalExpression in the pattern.
 * Based on Module 13 Interpreter Leadership Discussion Wiki example.
 */
public class Constant extends Value {

    private Integer value;

    public Constant(Integer value) {
        this.value = value;
    }

    public Integer evaluate() {
        return value;
    }
}
