package edu.jhu.apl.patterns_class.support.interpreter;

import java.util.LinkedList;
import java.util.List;

/**
 * Created for Assignment Module 13 Interpreter pattern.
 * Implements AbstractExpression in the pattern.
 * Based on Module 13 Interpreter Leadership Discussion Wiki example.
 */
public abstract class Operation extends Expression {
    protected List<Expression> childExpressions = new LinkedList<>();

    public void add(Expression e) {
        this.childExpressions.add(e);
    }
}