package edu.jhu.apl.patterns_class.support.interpreter;

/**
 * Created for Assignment Module 13 Interpreter pattern.
 * Implements TerminalExpression in the pattern.
 * Based on Module 13 Interpreter Leadership Discussion Wiki example.
 */
public class Variable extends Value {

    private Integer value;

    public Variable(Integer value) {
        this.value = value;
    }

    public Integer evaluate() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
