package edu.jhu.apl.patterns_class.support.interpreter;

/**
 * Created for Assignment Module 13 Interpreter pattern.
 * Implements AbstractExpression in the pattern.
 * Based on Module 13 Interpreter Leadership Discussion Wiki example.
 */
public abstract class UnaryOperation extends Operation {
    public void add(Expression e) {
        if (this.childExpressions.size() < 1)
            super.add(e);
    }
}