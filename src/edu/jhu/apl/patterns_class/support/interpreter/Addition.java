package edu.jhu.apl.patterns_class.support.interpreter;

/**
 * Created for Assignment Module 13 Interpreter pattern.
 * Implements NonterminalExpression in the pattern.
 * Based on Module 13 Interpreter Leadership Discussion Wiki example.
 */
public class Addition extends BinaryOperation {
    public Integer evaluate() {
        return this.childExpressions.get(0).evaluate() + this.childExpressions.get(1).evaluate();
    }
}