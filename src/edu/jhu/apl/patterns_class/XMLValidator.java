package edu.jhu.apl.patterns_class;

import java.io.IOException;
import java.util.Vector;

import org.w3c.dom.DOMException;

import edu.jhu.apl.patterns_class.dom.replacement.Element;
import edu.jhu.apl.patterns_class.dom.replacement.Node;
import edu.jhu.apl.patterns_class.dom.ElementInterface;
import edu.jhu.apl.patterns_class.dom.replacement.Attr;
import edu.jhu.apl.patterns_class.dom.replacement.Document;
import edu.jhu.apl.patterns_class.dom.replacement.Text;
import edu.jhu.apl.patterns_class.support.ValidatorMemento;
import edu.jhu.apl.patterns_class.support.validation.DocumentValidationDecorator;
import edu.jhu.apl.patterns_class.support.ValidChildren;

/**
 * Implements a configurable algorithm for checking whether or not an input XML
 * file follows a particular structure. Checks for illegal element types,
 * illegal element nesting, and illegal element attributes.
 */
public class XMLValidator {
	private Vector<ValidChildren> schema = new Vector<ValidChildren>();

	//
	// Supercedes any existing description for this element.
	//
	// Seems to be an instance of the Factory Method pattern.
	public ValidChildren addSchemaElement(String element) {
		ValidChildren schemaElement = findSchemaElement(element);

		if (schemaElement != null)
			schema.remove(schemaElement);

		schema.add(schemaElement = new ValidChildren(element));
		return schemaElement;
	}

	public ValidChildren findSchemaElement(String element) {
		// TODO: Pattern Opportunity: Factory, Decorator; May be able to do this
		// recursively, allowing hierarchy items to maintain state rather than the
		// validator.
		for (int i = 0; i < schema.size(); i++)
			// Return matching elm from schema
			if ((schema.elementAt(i).getThisElement() == null && element == null)
					|| (schema.elementAt(i).getThisElement() != null
							&& schema.elementAt(i).getThisElement().compareTo(element) == 0))
				return schema.elementAt(i);

		return null;
	}

	// TODO: Pattern Opportunity: Decorator. These `can` methods might be better as
	// Decorators on `schemaElement`.
	public boolean canRootElement(String newElement) {
		return canAddElement(null, newElement);
	}

	public boolean canAddElement(Element element, String newElement) {

		// Pattern Opportunity: Null Value.
		ValidChildren schemaElement = findSchemaElement(element == null ? null : element.getTagName());

		return schemaElement == null ? true : schemaElement.childIsValid(newElement, false);
	}

	public boolean canAddText(Element element) {
		ValidChildren schemaElement = findSchemaElement(element.getTagName());

		return schemaElement == null ? true : schemaElement.canHaveText();
	}

	public boolean canAddAttribute(Element element, String newAttribute) {
		ValidChildren schemaElement = findSchemaElement(element.getTagName());

		return schemaElement == null ? true : schemaElement.childIsValid(newAttribute, true);
	}

	//
	// Optional for schema implementation:
	//
	// public static boolean
	// canValue(Attribute attribute,
	// String value)
	// {
	// }

	/**
	 * Tests the program works.
	 *
	 * @param args Accepts 1 argument: The name of an output file.
	 */
	public static void main(String args[]) {
		if (args.length < 1) {
			System.out.println("No output filenames provided.");
			System.exit(0);
		}

		//
		// Create tree of this document:
		// <? xml version="1.0" encoding="UTF-8"?>
		// <document>
		// <element attribute="attribute value"/>
		// <element/>
		// <element attribute="attribute value" attribute2="attribute2 value">
		// Element Value
		// </element>
		// <element>
		// </element>
		// </document>
		//
		// Schema for this document:
		// document contains: element
		// element contains: element
		// element contains attributes: attribute, attribute2
		//
		// Pattern Opportunity: Chaining.
		XMLValidator xmlValidator = new XMLValidator();

		ValidChildren schemaRoot = xmlValidator.addSchemaElement(null);
		schemaRoot.addValidChild("document", false);

		ValidChildren schemaDocument = xmlValidator.addSchemaElement("document");
		schemaDocument.addValidChild("element", false);

		ValidChildren schemaElement = xmlValidator.addSchemaElement("element");
		schemaElement.addValidChild("element", false);
		schemaElement.addValidChild("attribute", true);
		schemaElement.addValidChild("attribute2", true);
		schemaElement.setCanHaveText(true);

		// Assignment Module 9: Added Memento. Testing setting and restoring state.
		ValidatorMemento memento = xmlValidator.createMemento();
		xmlValidator = new XMLValidator();
		xmlValidator.setMemento(memento);

		// Assignment 1: Decorate elements so they are validated when created.

		// Test adding elements to a document.
		Document vXmlDocument = new DocumentValidationDecorator(xmlValidator);

		Element document = vXmlDocument.createElement("document");

		Element element = vXmlDocument.createElement("element");
		Attr attribute = vXmlDocument.createAttribute("attribute");
		attribute.setValue("attribute value");
		element.setAttributeNode(attribute);
		document.appendChild(element);

		Element element2 = vXmlDocument.createElement("element");
		document.appendChild(element2);

		Element element3 = vXmlDocument.createElement("element");
		element3.setAttribute("attribute", "attribute value");
		element3.setAttribute("attribute2", "attribute2 value");
		Text text = vXmlDocument.createTextNode("Element Value");
		element3.appendChild(text);
		document.appendChild(element3);

		Element element4 = vXmlDocument.createElement("element");
		document.appendChild(element4);

		// Serialize the document to file.
		// Assignment Module 9: Disabled this to work-around bug with serialization introduced in earlier assignment.
		// try {
		// 	XMLSerializer xmlSerializer = new XMLSerializer(args[0]);
		// 	xmlSerializer.serializePretty(document);
		// 	xmlSerializer.close();
		// } catch (IOException e) {
		// 	System.out.println("Error writing file.");
		// 	e.printStackTrace();
		// }
	}


	/**
	 * Added for Assignment Module 9.
	 * Sets the Memento.
	 */
	public void setMemento (ValidatorMemento memento) {
		this.schema = memento.getState();
	}

	/**
	 * Added for Assignment Module 9.
	 * Creates the Memento.
	 */
	public ValidatorMemento createMemento() {
		ValidatorMemento memento = new ValidatorMemento();
		memento.setState(this.schema);
		return memento;
	}
}
