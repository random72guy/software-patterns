# Readme

This project contains code for the Software Patterns class project. The code will be updated throughout the year as assignments are released. Code is available from Git repository, upon request.

Class Diagrams for each refactor can be found in the `./class-diagrams` dir.


## Running the Code

**Prerequisites:** Java 8.

While the project can be run and built with the original instructions (see `./src/README.txt`), Gradle has been added to accomodate Windows, simplify the build/run process, and ensure consistency across runs.

There are three main functions: Serialize, Tokenize, and Validate. Each of these can be run (from the project root dir) as follows:

```
./gradlew runSerializer
./gradlew runTokenizer
./gradlew runValidator
```

Note: Gradle need not be installed if the Gradle wrapper (`./gradlew`) is used.

To customize run configurations (including arguments), see `./build.gradle`.\

To run in debug mode, append any of the above run commands with `--debug-jvm` and attach your Java debugger to the indicated port.


## Change Log

The high-level changes made for each assignment are noted here. (See Git history for more details.)

The source code contains comments referencing design patterns that have been or that may eventually be impelmented for assignments. Changes made for assignments are marked as `Assignment <number>`. Potential opportunities for implmenting patterns are flagged as `Pattern Opportunity`.


### Assignment 1

#### Assignment

1. Add Factory Methods as necessary to create DOM or other objects.
2. The XMLSerializer class implements two different serialization algorithms. There are three opportunities for applying Strategy to this class: extracting the necessary data from each node in the DOM tree, inserting white space according to the desired output format, and selecting and initializing the output stream. Implement at least the first of these and one of the others. Two of the Strategy implementations do not require the creation of any classes not already present in the framework.
3. The XMLValidator class that allows the parser to determine whether or not an input document follows some particular structure, i.e., what kinds of elements are permissible, what elements may contain other elements and which kind, and what attributes are permitted in each element. (In a proper XML implementation this is Schema or DTD validation.) Decorate the DOM classes to distribute the algorithm among them. (Consider following Kerievsky’s refactoring “Move Embellishment to Decorator” where the validation process is the embellishment to be moved. http://www.industriallogic.com/xp/refactoring/embellishmentToDecorator.html)


#### Changes

* `Serializer.serializePretty` adjusted to use a strategy for reading and writing DOM elements. Necessitated creation of the `SerializationStrategy` class, which has a Factory Method for generating the correct ConcreteStrategy. Also adjusted to use a strategy (with a factory) for whitespace insertion.
* Decorators added to `Validator.main()` for schema validation.


### Assignment 2

#### Assigment

1. Make any modifications necessary to the DOM interfaces and implementation to make the DOM tree fit the Composite pattern. Consider following Kerievsky’s refactoring “Extract Composite” to move methods out of subclasses.
2. Extract the recursive traversal logic from the original XMLSerializer class into an Iterator.


#### Changes

* Converted `Node` to an abstract class to reflect `Component` in the Composite pattern.
* Created `NodeComposite` and `NodeLeaf` classes to represent `Composite` and `Leaf` in the Composite pattern. Each implements the child-related methods in `Node`. `Element` and `Document` extend `NodeComposite`, and `Text` and `Attr` extend `NodeLeaf`.
* Adjusted `Serializer` `serializeMinimal()` and `serializePretty()` to use new `NodeIterator`. Also refactored `Element` `Attr` iteration to use a standard iterator (rather than a for-loop).
* Consolidated pretty and minimal print methods, and removed whitespace insertion strategy (as it was verbose and difficult to reconcile with the new Iterator pattern).

### Assignment 3

#### Assignment

1. Reimplement the serialization algorithm or some part of it, either the original or your Strategy, using Template Method, by identifying the variant and invariant parts of the algorithm.
2. Implement a Builder using XMLTokenizer to construct a DOM tree. Use Abstract Factory to create the Builder’s object creation component. (I won’t grade on the bugginess of your parser. I’m interested in the Builder.)

#### Changes

1. Implemented the Template Method in the Serializer by adding `AbstractSerializationTemplate` and concrete classes.
2. In Tokenizer, implemented Builder pattern via `DOMDirector`, `AbstractDOMBuilder`, and `ConcreteDOMBuilder`. Builder relies on a new `AbstractDOMFactory` and the `ConcreteDOMFactory` to create nodes, andthen assembles the nodes into a DOM tree. The test script was also updated to serialize the tree to `TestTokenizer.txt`. (The pre-existing tokenizer does not appear to correctly strip certain characters like "=", resulting in an invalid serialized output file.)


### Assignment 4

#### Assignment

1. Redesign the Abstract Factory or Builder classes from the Module 4 assignment as a **Singleton** or **Monostate**.
2. The SAX API for representing XML delays parsing of parts of the XML file until the application actually requires them. Achieve part of that behavior by implementing a virtual Proxy for Element that allows deferring parsing of the Element’s children until they are needed. (The Proxy will need to keep some kind of reference into the file for where it needs to resume parsing when the real Element is required.)

#### Changes & Discussion

0. `AbstractDOMFactory`, `ConcreteDOMFactory`, `ConcreteDOMBuilder`, `AbsstractDOMBuilder`, and `DOMDirector` moved to their own files.
1. `ConcreteDOMFactory` is already Monostate, by technicality: it has no non-static instance methods, since it has no instance methods. No diagram is provided, as the relationship between classes and objects in the application has not significantly changed.
2. Implemented an `ElementProxy` class which implements `Element`. A couple of new interfaces had to be created so that `ElementProxy` and `Element` could implement the same interface: `ElementInterface` and `NodeCompositeInterface`. `ElementInterface` represents the `Subject`, `Element` the `RealSubject`, and `ElementProxy` the `Proxy`. The Builder creates proxies. When member methods are called on the proxy that requires an `Element`, the element is generated by reading the input file using the Builder, Director, and Serializer. There is a null-pointer bug with loading data from the virtual proxy during serialization; therefore, the serialization part of the test has been commented-out.
3. In testing interface changes, it seems the Validator was broken during Assignment 2.

### Assignment 5

#### Assignment

1. Research an idiom for the language in which you are doing the pattern assignments (Java) and apply it to the application.

#### Changes & Discussion

1. Applied [Helper Method for Initializations](http://wiki.c2.com/?HelperMethodForInitializations) idiom to following classes: `XMLSerializer`, `XMLTokenizer`. Considered adding to DOM elements, but none seemed to have meaningful logic that would warrant this.

### Assignment Module 7

#### Assignment

1. Create an Adapter or Bridge to use the real `org.w3c.dom` structure as a wrapper around the `Node` and `Document` members of the `edu.jhu.apl.patterns_class.dom` DOM structure. Don’t worry about trying to create methods for parts of the `org.w3c.dom` which do not have implementations in the `patterns_class.dom`.
2. Describe the reason for choosing the pattern you selected and for not choosing the other. Include the answer in a `Readme.txt` file along with the implementation of part 1.

#### Changes & Discussion

1. Two classes (and associated files) were created: `NodeToW3CNodeAdapter` and `DocumentToW3CDocumentAdapter`. These serve as the Adapter in the Object Adapter version of the pattern, and adapt the existing JHU Node / Document (Adaptee) to the official W3C Node / Document (Target). Since none of our main project code uses the W3C interfaces, there is no Client participant; however, our adapter would allow any application / Client designed to interact with the W3C interfaces to operate with our implementations.
2. Due to the interdependnece of the classes involved, any attempt to implement a Bridge would require the abstraction and implementation hierarchies to be connected; they wouldn't be able to vary independently, defeating the purpose - and violating the structure - of a Bridge. A set of Adapters would be required. It's unclear that a Bridge is desireable; we don't have an abstraction hierarchy that needs to differ, since our hierarchy is the same as the W3C's.

### Assignment Module 8

#### Assignment

1. Apply Observer to observe the actions performed by the Builder as it constructs a DOM structure as if you were going to provide parsing status in a GUI. It is sufficient to log the Observers updates to stdout.
2. Apply Mediator to your Observer implementation allowing the Observers to communicate with each other to exchange state of the validation and parsing process. I let the Observers signal activation to each other.

#### Changes & Discussion

1. Created `Subject` abstract class and `Observer` interface in the new `support` dir.
2. Created `MockGUI` and `MockGUIComponent` classes in the new `support` dir.
3. `XMLTokenizer` now creates a `MockGUI` for its `ConcreteDOMBuilder`. The `MockGUI` creates `MockGUIComponents` which observe particular `ConcreteDOMBuilder` construction events and then 1) print to console, and b) notify other `MockGUIComponents` that an event was received via the `MockGUI`, which acts as a mediator to transmit the message to all its components.
4. See classes' JavaDoc and implementations for more details.


### Assignment Module 9

#### Assignment

1. Using the supplied XML file as a configuration example (or make up your own), apply Chain of Responsibility to the DOM tree as an event handler. Allow events to be received by any element and then handled by the appropriate element in the tree. Note that this pattern's requirements can be implemented with no more than a couple of dozen lines of code.
2. Create a Memento implementation that preserves the configuration of the `XMLValidator` class.


#### Changes & Discussion

* Added `EventHandling.xml` file.
* Implemented Memento pattern to save the `XMLValidator` validation DOM.
  * `XMLValidator` is the Originator, and its `main()` test method is the Caretaker. (This makes `XMLValidator` both Originator and Caretaker.) While this departs from the canonical Memento structure, there is no logical alternate Caretaker in the current implementation, and any additional class would introduce additional complexity without real functional benefit. This implementation decision has the additional consequence that only a wide Memento interface is required.
  * Created `ValidatorMemento` to act as the Memento in the pattern.
  * Needed relocate the `ValidChildren` class to its own file to resolve visibility issues.
  * To test the memento functionality, the `XMLValidator` created during the `XMLValidator` test is simply wiped and then loaded from the memennto.
  * Disabled serialization in the Validator test to avoid bug with Serialization introudced in a previous assignment where `Document` sometimes has no child nodes.
* Implemented Chain of Responsibility pattern.
  * Added logic in `XMLTokenizer` to test capability.
  * Added `sendEvent()` method to `NodeComposite`. This method checks if the node should handle the sent events; if not, it sends the events to its children.
  * Had to fix bugs in the stock tokenizer, as it was providing improper values.
  * The implementation appears to work, but there seems to be an existing bug with the structure of the generated DOM tree, resulting in trees not having all the child elements expected.
  * No Class Diagram is supplied for this implementation, as it would be trivial. The client (`XMLTokenizer`) sends events to `NodeComposite` objects organized hierarchically, which send them to their children of the same class. `NodeComposite` is the abstract Handler, and contains an implementation for the handling logic.

### Assignment Module 11

#### Assignment

1. Add a simple command interpreter to your parser application allowing the program to be controlled dynamically instead of solely from the command line. Use Command to implement the operations. Examples of commands might be parse a file, serialize a tree or subtree by one or another method, change or add an attribute, etc.
2. Use Prototype to duplicate a DOM (sub)tree.


#### Changes & Discussion

1. Command Interpreter
    * New class `XMLParserCLI` has a `main` method that acts as the Client, creating ConcreteCommand objects and configuring their receivers. For convenienence, it also configures the Invoker (an instantiated `XMLParserCLI`) to use these ConcreteCommand objects.
    * An instantiated `XMLParserCLI` acts as the Invoker. It listens for requests and calls the corresponding commands that have been configured by the Client (`XMLParserCLI.main()`).
    * Created proof-of-concept Command class in the `support/commands` directory: `ParseFileCommand` and `EditNodeValCommand`. These represent the Command interface, and, when instantiated, represent ConcreteCommands.
    * `XMLTokenizer` and `NodeCompositeInterface` act as the commands' respective Receivers in the pattern.
2. DOM Tree Duplication
    * Added a `duplicate()` method to the `Node` interface (the Prototype in the pattern) and implementations to `Document`, `Element`, `Text`, and `Attr`, which act as ConcretePrototypes and call `duplicate()` recursively to deep-clone the nodes in teh DOM tree.
        * The method name "duplicate" was chosen to avoid clashing with Java's native `Object.clone()` method.
    * Invoke the new `duplicate()` method from the `XMLTokenizer` main method, which acts as the Client in the pattern.


### Assignment Module 12

#### Assignment

1. Apply State to your Builder pattern to implement the various states of the parser, e.g., InsideElementTag, InsideAttribute as you call them.
2. Modify your XMLSerializer to use Visitor instead of direct calls to Node.serialize from the Strategy pattern implementation.

#### Changes & Discussion

1. State
    * Created `BuildState` class, the State participant in the pattern.
    * Created ConcreteState classes: `AfterElementBuildState`, `DoneBuildState`, `InsideAttrBuildState`, `InsideDocumentBuildState`, `InsideElementBuildState`, `InsideTextBuildState`. Their `handle()` method is supplied with a reference to `BuildContext`, which has information they require in order to operate.
    * Created `BuildContext` class to implement Context in the pattern. It maintains information about the state of the DOM construction process that the ConcreteState classes require. It also maintains the current state, and can adjust its state as required. (Arguably it would have been more appropriate to let the ConcreteStates adjust the active state; however, since each ConcreteState must set a new state after execution, it was simpler to execute this change logic within `BuildContext`.)
    * Modified `DOMDirector` to use `BuildContext.build()` to build the project. Logic was moved from `DOMDirector`: state-dependent code was moved to the new ConceteState classes, and class-independent code was implemented in `BuildContext`.
2. Visitor
    * Replacement `Node` is the Element in the pattern.
    * `Node` is the ConcreteElement in the pattern.
    * `Node` exposes an iterator interface for traversing its hierarchy. This hierarchy is the ObjectStructure in the pattern.
    * Created `NodeVisitor`, implementing Visitor in the pattern.
    * Created `SerializationNodeVisitor`, implementing the ConcreteVisitor in the pattern.
    * Moved Serialization code from `XMLSerializer` to new `SerializationStrategy` class to make it more accessible to `SerializationNodeVisitor`.
    * Responsibility for traversal was given to the ConcreteVisitor `SerializationNodeVisitor`, though the iterator is provided by `XMLSerializer`. This was done primarily to retain similarity to - and minimize effort adjusting - the former implemenation. No compelling reason was recognized to control the iteration some other way.
    * It seemed unnecessary to create other visitors for this implementation. However, this

### Assignment Module 13

#### Assignment

1. Modify the internal representation of the DOM objects to store them as Flyweights. Clearly identify what state is extrinsic and intrinsic in each Node type.
2. Make any changes necessary to the output of the parser to implement the Interpreter pattern. Similar to (prof's implementation of) Chain of Responsibility, this patterns requirements can be implemented with a small amount of code.  Assume a document structure similar to this, which is interpreted to "-5":
```
<xml version="1.0" encoding="UTF-8"?>
<operation type="-">
    <operation type="+">
        <value>2</value>
        <value>3</value>
    </operation>
</operation>
```
3. Wrap the entire application in a Façade encapsulating all of the functionality behind a single interface. (From lecture: Load, Serialize, prototypes for new operations.)

#### Changes & Discussion

1. Flyweight:
    * `Attr` and `Text` were chosen for our Flyweights. Each has intrinsic state that could be shared: `Attr` has `name` and `value`, while `Text` has just `value`. Their extrinsic state is their position in the DOM; since they are leaves, they each have a `parent` - but no `children` - and a `document`, which we can assume will be the same for all nodes (assuming we only support working with one document at a time per Flyweight Factory). `Document` is an unsuitable Flyweight because it's never duplicated. `Element` is unsuitable because, via its child elements, it defines the structure of all the DOM elements (i.e. the extrinsic state of `Attr` and `Text`), and it is unclear that the structure could be stored more efficiently in an external structure (and doing so would require massive refactoring of the application).
    * `ConcreteDOMFactory` is the `FlyweightFactory` in the pattern. It was adjusted to leverage a flyweight pool.
    * `Attr` and `Text` were modified to implement `ConcreteFlyweights` in the pattern; the replacement interfaces `Attr` and `Text` implement Flyweight.
    * `Attr` and `Text` extend `Node`, must implement some methods that require extrinsic state without these signatures being modified to accept that state as an argument. Some methods are impractical to provide an extrinsic state, such as `getOwnerElement()`, which is presumably called in order to determine the parent that would need to be supplied. There are also methods inherited from `Node` which cannot be modified to accept extrinsic state without changing all the DOM elements' interfaces; fortunately, most of these deal with child nodes and do not need to return a value.
    * Created `NodeLeafFlyweight` to handle inherited `Node` methods requiring extrinsic state.
        * These were mostly modified to return `null`. (Null appears to be supported by the existing application logic, meaning a null object was not required.)
        * Most `Attr` and `Text` methods did not require extrinsic state. However, `NodeLeafFlyweight.isSameNode()` leverages extrinsic state, as position is required to determine equality.
        * In a slight deviation from the pattern, some operation logic is handled in the `NodeLeafFlyweight`; this avoids duplication of identical logic in the ConcreteFlyweights.
2. Parser:
    * Created expression classes in the `support/interpreter` dir, modeled on the Module 13 Interpreter Leadership Discussion Wiki example.
    * Added code to the `XMLTokenizer` main test method to interperet the XML file results.
3. Facade:
    * Created `Facade` class, implementing Facade in the eponymous pattern. Facade provides methods facilitating the most common use cases of the application: serializing a document to file, tokenizing a file to a DOM tree, and validating an XML document.
        * The facade required moving validation classes from the `XMLValidator` to their own files in the `support` dir.
